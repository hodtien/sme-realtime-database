package middleware

import (
	"crypto/rand"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/labstack/echo"
)

//ValidateDeviceID - Kiểm tra Device ID là có tồn tại trong request header trước khi handling api
func ValidateDeviceID(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		deviceID := c.Request().Header.Get("deviceid")
		if deviceID == "" {
			return c.JSON(http.StatusBadRequest, "UserID or DeviceID is Invalid")
		}
		return next(c)
	}
}

// ValidateRateLimit - ValidateRateLimit
func ValidateRateLimit(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		limiter := limiter.GetLimiter(c.Request().RemoteAddr)
		if !limiter.Allow() {
			return c.JSON(http.StatusTooManyRequests, http.StatusText(http.StatusTooManyRequests))
		}
		return next(c)
	}
}

//ValidateUserID - Kiểm tra User ID là có tồn tại trong request header trước khi handling api
func ValidateUserID(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		userID := c.Request().Header.Get("userid")
		if userID == "" {
			return c.JSON(http.StatusBadRequest, "UserID or DeviceID is Invalid")
		}
		return next(c)
	}
}

//ValidateAPIKey - ValidateAPIKey
func ValidateAPIKey(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Request().Header.Get("Authorization") == "" || len(strings.Split(c.Request().Header.Get("Authorization"), " ")) < 2 {
			return c.JSON(http.StatusBadRequest, "Token is Invalid")
		}
		jwtReqHeader := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]

		if db.Exists(jwtReqHeader) {
			return c.JSON(http.StatusBadRequest, "Token is Invalid")
		}

		jwtMapClaim, err := extractClaims(jwtReqHeader)
		if !err || isNil(jwtMapClaim) || isNil(jwtMapClaim["userID"]) || isNil(jwtMapClaim["deviceID"]) {
			return c.JSON(http.StatusBadRequest, "Token is Invalid")
		}

		if !db.Exists("DM" + "$" + jwtMapClaim["userID"].(string) + "$" + jwtMapClaim["deviceID"].(string)) {
			return c.JSON(http.StatusBadRequest, "Token is Invalid")
		}
		return next(c)
	}
}

//GetUserIDAndDeviceIDByAPIKey - Lấy về User ID & Device ID từ API Key
func GetUserIDAndDeviceIDByAPIKey(apikey string) (userID string, deviceID string, err error) {
	jwtMapClaim, _ := extractClaims(apikey)
	return jwtMapClaim["userID"].(string), jwtMapClaim["deviceID"].(string), nil
}

// DestroyJWT - Destroy a JWT
func DestroyJWT(jwt string) bool {
	_, err := db.Set(jwt, "blacklist")
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

// InitialMinioClient - InitialMinioClient
// func InitialMinioClient() {
// 	fmt.Println("Check and Download Minio Client")
// 	if _, err := os.Stat("/app"); os.IsNotExist(err) {
// 		os.Mkdir("/app", 0777)
// 	}
// 	if _, err := os.Stat("/app/mc"); os.IsNotExist(err) {
// 		err := DownloadFile("https://dl.min.io/client/mc/release/linux-amd64/mc", "/app/mc")
// 		if err != nil {
// 			fmt.Println("Download Minio Client Failed")
// 		} else {
// 			fmt.Println("Download Minio Client Success")
// 			os.Chmod("/app/mc", 0777)
// 			cmd := exec.Command("/app/mc", "config", "host", "add", "vnptminio", "http://203.162.141.37:9000", "minioadmin", "minioadmin")
// 			out, err := cmd.CombinedOutput()
// 			if err != nil {
// 				fmt.Printf("cmd.Run() failed with %s\n", err)
// 				return
// 			}
// 			fmt.Printf(string(out))
// 		}
// 	}

// }

// MinioCreateUser - Khởi tạo Minio User
// func MinioCreateUser(userID string) error {
// 	// Check Minio User is Exists
// 	cmd := exec.Command("/app/mc", "admin", "user", "info", "vnptminio", "minio_"+userID)
// 	out, err := cmd.CombinedOutput()
// 	if err == nil {
// 		fmt.Printf("Minio User is Exists")
// 		return nil
// 	}
// 	fmt.Printf("Minio User is not Exists. Creating Minio User")

// 	// Create Minio User
// 	cmd = exec.Command("/app/mc", "admin", "user", "add", "vnptminio", "minio_"+userID, "minio_"+userID)
// 	out, err = cmd.CombinedOutput()
// 	if err != nil {
// 		fmt.Printf("cmd.Run() failed with %s\n", err)
// 		return err
// 	}
// 	fmt.Printf(string(out))

// 	fmt.Printf(string(out))
// 	return nil
// }

//GenerateRandomWithDigit - GenerateRandomWithDigit
func GenerateRandomWithDigit(max int) string {
	b := make([]byte, max)
	n, err := io.ReadAtLeast(rand.Reader, b, max)
	if n != max {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	return string(b)
}
