package middleware

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	//Key - Key JWT Token
	Key = "vnpt"
)

// IsNumber - check string is Number
func IsNumber(s string) bool {
	match, _ := regexp.MatchString("([a-z]+)", s)
	if !match {
		return true
	}
	return false
}

// DeleteEmpty - Delete All Empty Element in Array
func DeleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

// GenerateJwtToken -  Generate a Jwt Token
func GenerateJwtToken(data string) (t string, err error) {
	//----- JWT -----//
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = data
	claims["exp"] = time.Now().Add(60 * time.Minute).Unix() // Default Token Login is 60 minutes

	// Generate encoded token and send it as response
	t, err = token.SignedString([]byte(Key))
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	return t, nil
}

// GenerateDeviceAPIKey -  Generate a Device API Key
func GenerateDeviceAPIKey(userID, deviceID string) (t string, err error) {
	//----- JWT -----//
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["userID"] = userID
	claims["deviceID"] = deviceID
	claims["exp"] = time.Now().AddDate(1, 0, 0).Unix() // Default Device API Key is 1 year

	// Generate encoded token and send it as response
	t, err = token.SignedString([]byte(Key))
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	return t, nil
}

func extractClaims(tokenStr string) (jwt.MapClaims, bool) {
	hmacSecretString := Key // Value
	hmacSecret := []byte(hmacSecretString)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return hmacSecret, nil
	})

	if err != nil {
		fmt.Println(err)
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	}
	fmt.Println("Invalid JWT Token")
	return nil, false
}

func isNil(v interface{}) bool {
	return v == nil || (reflect.ValueOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).IsNil())
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

//ConvertBytesToMap - Convert Data Bytes to Map type
func ConvertBytesToMap(bodyBytes []byte) (map[string]interface{}, error) {
	data := make(map[string]interface{})
	err := json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return data, err
}

// Sizer - Size File Interface
type Sizer interface {
	Size() int64
}

// DownloadFile - DownloadFile
func DownloadFile(url string, filepath string) error {
	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
