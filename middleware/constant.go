package middleware

import (
	"regexp"

	"bitbucket.org/cloud-platform/sme-realtime-database/repository"
)

var limiter = NewIPRateLimiter(500, 5)
var db repository.Cache
var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

// IsStringAlphabetic - IsStringAlphabetic
var IsStringAlphabetic = regexp.MustCompile(`^[a-zA-Z0-9_]*$`).MatchString
