package utils

import (
	"fmt"
	"time"

	mq "bitbucket.org/cloud-platform/sme-realtime-database/transport"
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

// KafkaPublishSaveOrUpdate - KafkaPublishSaveOrUpdate
func KafkaPublishSaveOrUpdate(producer *kafka.Producer, apiKey, collection, record, data string) error {
	time.Sleep(100 * time.Millisecond)
	consumerSaveCmdTopic := "SaveOrUpdate"
	keyMsgKafka := collection + "," + record
	valueMsgKafka := data
	//fmt.Println("Publish Msg to " + consumerSaveCmdTopic + " topic Kafka: \nKey: " + keyMsgKafka + "\nValue: " + valueMsgKafka)
	if err := mq.PublishMessage(producer, consumerSaveCmdTopic, 0, keyMsgKafka, valueMsgKafka); err != nil {
		fmt.Println(err)
		return err
	}
	// if apiKey != "" {
	// 	HTTPSynchronizeRTDB(apiKey, []byte(errCode15))
	// }
	return nil
}

// KafkaPublishDelete - KafkaPublishDelete
func KafkaPublishDelete(producer *kafka.Producer, apiKey, collection, record string) error {
	time.Sleep(100 * time.Millisecond)
	consumerDeleteCmdTopic := "Delete"
	keyMsgKafka := collection
	valueMsgKafka := record
	//fmt.Println("Publish Msg to " + consumerDeleteCmdTopic + " topic Kafka: \nKey: " + keyMsgKafka + "\nValue: " + valueMsgKafka)
	if err := mq.PublishMessage(producer, consumerDeleteCmdTopic, 0, keyMsgKafka, valueMsgKafka); err != nil {
		fmt.Println(err)
		return err
	}
	// if apiKey != "" {
	// 	HTTPSynchronizeRTDB(apiKey, []byte(errCode12))
	// }
	return nil
}

// KafkaPublishDrop - KafkaPublishDrop
func KafkaPublishDrop(producer *kafka.Producer, apiKey, key, value string) error {
	time.Sleep(100 * time.Millisecond)
	consumerDropCmdTopic := "Drop"
	keyMsgKafka := key
	valueMsgKafka := value
	//fmt.Println("Publish Msg to " + consumerDropCmdTopic + " topic Kafka: \nKey: " + keyMsgKafka + "\nValue: " + valueMsgKafka)
	if err := mq.PublishMessage(producer, consumerDropCmdTopic, 0, keyMsgKafka, valueMsgKafka); err != nil {
		fmt.Println(err)
		return err
	}
	// if apiKey != "" {
	// 	HTTPSynchronizeRTDB(apiKey, []byte(errCode12))
	// }
	return nil
}

// KafkaPublishEvent - KafkaPublishEvent
func KafkaPublishEvent(producer *kafka.Producer, header, data string) error {
	time.Sleep(100 * time.Millisecond)
	consumerSaveCmdTopic := "EventStore"
	keyMsgKafka := header
	valueMsgKafka := data
	if err := mq.PublishMessage(producer, consumerSaveCmdTopic, 0, keyMsgKafka, valueMsgKafka); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
