package utils

// PublishData - Publish Data Structure
type PublishData struct {
	Method string     `json:"method"`
	Params ParamsData `json:"params"`
}

// ParamsData - Child Structure of PublishData
type ParamsData struct {
	Channel string      `json:"channel"`
	Data    interface{} `json:"data"`
}

const (
	errCode1  = "1"  // For API /api/core/v1/data/detail/:bucket/*
	errCode2  = "2"  // For API /api/core/v1/data/all_in_bucket/:bucket
	errCode3  = "3"  // For API /api/core/v1/data/all
	errCode4  = "4"  // For API /api/core/v1/data/list/:bucket
	errCode5  = "5"  // For API /api/core/v1/data/all/search/:bucket
	errCode6  = "6"  // For API /api/core/v1/data/all_in_bucket/sort/:bucket
	errCode7  = "7"  // For API /api/core/v1/data/all_in_bucket/limit/:bucket
	errCode8  = "8"  // For API /api/core/v1/data/all_in_bucket/pagging/:bucket
	errCode9  = "9"  // For API /api/core/v1/data/all_in_bucket/match/:bucket
	errCode10 = "10" // For API /api/core/v1/data/create/:bucket/:record
	errCode11 = "11" // For API /api/core/v1/data/retrieve_many/:bucket
	errCode12 = "12" // For API /api/core/v1/data/delete/:bucket/*
	errCode13 = "13" // For API /api/core/v1/data/update_one/:bucket/:record/*
	errCode14 = "14" // For API /api/core/v1/update_many/:bucket/:record
	errCode15 = "15" // For API /api/core/v1/data/update_field/:bucket/:record
	errCode16 = "16" // For API /api/core/v1/bucket/detail/*
	errCode17 = "17" // For API /api/core/v1/bucket/all
	errCode18 = "18" // For API /api/core/v1/bucket/all_in_device
	errCode19 = "19" // For API /api/core/v1/bucket/list
	errCode20 = "20" // For API /api/core/v1/bucket/list_record/:bucket
	errCode21 = "21" // For API /api/core/v1/bucket/create/:bucket
	errCode22 = "22" // For API /api/core/v1/bucket/delete/:bucket
	errCode23 = "23" // For API /api/core/v1/bucket/update/:bucket
	errCode24 = "24" // For API /api/core/v1/minio/object/all
	errCode25 = "25" // For API /api/core/v1/minio/bucket/all
	errCode26 = "26" // For API /api/core/v1/minio/bucket/list
	errCode27 = "27" // For API /api/core/v1/minio/create/:bucket
	errCode28 = "28" // For API /api/core/v1/minio/upload/:bucket
	errCode29 = "29" // For API /api/core/v1/minio/delete_bucket/:bucket
	errCode30 = "30" // For API /api/core/v1/minio/delete_object/:bucket/:record
	errCode31 = "31" // For API /api/core/v1/vnpt/notify/:device_id
	errCode32 = "32" // For API /api/core/v1/firebase
)
