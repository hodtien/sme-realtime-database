package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
)

//CentrifugoHost - Centrifugo Host URL
const CentrifugoHost string = "http://221.132.29.81:8000"

// HTTPSynchronizeRTDB - HTTPSynchronizeRTDB
func HTTPSynchronizeRTDB(apiKey string, bodyBytes []byte) error {

	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apiKey)
	if deviceID == "" || userID == "" || err != nil {
		fmt.Println("Authorization is Invalid")
		return err
	}

	pubData := new(PublishData)
	pubData.Method = "publish"
	pubData.Params.Channel = apiKey
	pubData.Params.Data = string(bodyBytes)
	bodyData, err := json.Marshal(pubData)
	if err != nil {
		fmt.Println("Push Notification Failed")
		return err
	}

	req, err := http.NewRequest("POST", CentrifugoHost+"/api", bytes.NewBuffer(bodyData))
	if err != nil {
		fmt.Println("Error reading request. ", err)
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "apikey 3b541795-fabe-41bd-abd7-4ae848bc300d")

	client := &http.Client{Timeout: time.Second * 20}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error reading response. ", err)
		return err
	}
	defer resp.Body.Close()

	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading body. ", err)
		return err
	}
	fmt.Println("Success Send Notification for Synchronize DB Realtime")
	return nil
}
