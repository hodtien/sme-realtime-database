# build stage
FROM golang as builder

# librdkafka Build from source
RUN git clone https://github.com/edenhill/librdkafka.git
WORKDIR librdkafka
RUN ./configure --prefix /usr
RUN make
RUN make install

# Build go binary

RUN mkdir -p /go/src/bitbucket.org/cloud-platform/sme-realtime-database
WORKDIR /go/src/bitbucket.org/cloud-platform/sme-realtime-database

# copies the Gopkg.toml and Gopkg.lock to WORKDIR
COPY . .

# install the dependencies without checking for go code
RUN go get .

# Build my app
RUN go build -o sme-realtime-database

# final stage
FROM ubuntu
COPY --from=builder /usr/lib/pkgconfig /usr/lib/pkgconfig
COPY --from=builder /usr/lib/librdkafka* /usr/lib/
# Set the Current Working Directory inside the container
WORKDIR /app

# Build Args
ARG LOG_DIR=/app/logs

# Create Log Directory
RUN mkdir -p ${LOG_DIR}

# Environment Variables
ENV LOG_FILE_LOCATION=${LOG_DIR}/app.log

# This container exposes port 1323 to the outside world
EXPOSE 1323

COPY --from=builder /go/src/bitbucket.org/cloud-platform/sme-realtime-database/sme-realtime-database /app
COPY --from=builder /go/src/bitbucket.org/cloud-platform/sme-realtime-database/config.json /app

# Declare volumes to mount
VOLUME [${LOG_DIR}]

# Run the binary program produced by `go install`
CMD /app/sme-realtime-database