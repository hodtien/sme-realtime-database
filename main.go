package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	statistic "bitbucket.org/cloud-platform/sme-realtime-database/analytics"
	mdw "bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/repository"
	authentication "bitbucket.org/cloud-platform/sme-realtime-database/service/authentication_component"
	queue "bitbucket.org/cloud-platform/sme-realtime-database/service/queue_component"
	rtdb "bitbucket.org/cloud-platform/sme-realtime-database/service/realtime_db_component"
	recordMgnt "bitbucket.org/cloud-platform/sme-realtime-database/service/realtime_db_component/record_management"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
	"gopkg.in/go-playground/validator.v9"
)

var db repository.Cache
var mgodb repository.Mgo

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	if viper.GetString(`swift.active`) == "true" {
		rtdb.SwiftInit()
		fmt.Println("Swift Initial Success")
	}
	if viper.GetString(`kafka.active`) == "true" {
		kafkaURL := viper.GetString(`kafka.addr`)
		rtdb.KafkaInit(kafkaURL)
		fmt.Println("Kafka Initial Success")
	}
	redisURL := viper.GetString(`redis.url`)
	maxClients := viper.GetInt(`redis.max_clients`)
	minIdle := viper.GetInt(`redis.min_idle`)
	db.CreateCache(redisURL, maxClients, minIdle)
	mongoURL := viper.GetString(`mongodb.url`)
	mongoDBName := viper.GetString(`mongodb.dbname`)
	mgodb.InitialDatabase(mongoURL, mongoDBName)
}

func main() {
	fmt.Println("CPU CORE USING: ", runtime.NumCPU())
	e := echo.New()
	s := &http.Server{
		Addr:         ":9990",
		ReadTimeout:  20 * time.Minute,
		WriteTimeout: 20 * time.Minute,
	}
	e.Validator = &rtdb.CustomValidator{Validator: validator.New()}

	// gValidateAPIKey := e.Group("", mdw.ValidateRateLimit, mdw.ValidateAPIKey)
	// eGroup := e.Group("", mdw.ValidateRateLimit)

	gValidateAPIKey := e.Group("", mdw.ValidateAPIKey)
	eGroup := e.Group("")

	//Enable CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*", "http://221.132.29.81:8080", "http://221.132.29.81:1325", "http://221.132.29.81:1325/*", "http://221.132.29.81:1323", "http://221.132.29.81:1323/*"},
		AllowCredentials: true,
		AllowHeaders:     []string{http.MethodOptions, http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete, echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, "Authorization", "Content-Type", "Bearer", "Bearer ", "content-type", "Origin", "Accept", "Referer", "User-Agent", "userid", "deviceid"},
	}))
	//e.Use(middleware.CORS())
	gValidateAPIKey.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*", "http://221.132.29.81:8080", "http://221.132.29.81:1325", "http://221.132.29.81:1325/*", "http://221.132.29.81:1323", "http://221.132.29.81:1323/*"},
		AllowCredentials: true,
		AllowHeaders:     []string{http.MethodOptions, http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete, echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, "Authorization", "Content-Type", "Bearer", "Bearer ", "content-type", "Origin", "Accept", "Referer", "User-Agent", "userid", "deviceid"},
	}))

	/****************************** STATISTIC API *****************************/
	eGroup.GET("/api/core/v1/stats_by_date/:date", statistic.StatsDetail)
	eGroup.GET("/api/core/v1/stats_summary", statistic.StatsSummary)
	eGroup.GET("/api/core/v1/analytics/stats_by_day/:day", statistic.StatsByDayReport)
	eGroup.GET("/api/core/v1/analytics/stats_by_month/:month", statistic.StatsByMonthReport)
	eGroup.GET("/api/core/v1/analytics/stats_by_year/:year", statistic.StatsByYearReport)

	/****************************** USER MANAGEMENT API *****************************/
	// API Tạo mới một User cho trình quản lý User của mBaaS Platform
	eGroup.POST("/api/core/v1/user/register/:bucket_id", authentication.UserRegister)
	// API Đăng nhập User cho trình quản lý User của mBaaS Platform
	eGroup.POST("/api/core/v1/user/login/:bucket_id", authentication.UserLogin)
	// API Xoá User cho trình quản lý User của mBaaS Platform
	eGroup.DELETE("/api/core/v1/user/delete/:bucket_id", authentication.DeleteUser)

	/****************************** DEVICE API *****************************/
	// API Lấy một Device cụ thể trong Bucket
	eGroup.GET("/api/core/v1/device/detail/*", rtdb.RetrieveDevice)
	// API Lấy toàn bộ Devices
	eGroup.GET("/api/core/v1/device/all", rtdb.RetrieveAllDevice)
	// API Lấy danh sách và số lượng Device
	eGroup.GET("/api/core/v1/device/list", rtdb.RetrieveNumberDevices)
	// API Tìm kiếm các gía trị của Device theo Field:Value trong DB
	eGroup.GET("/api/core/v1/device/search", rtdb.SearchDevice)
	// API lấy về danh sách Bucket thuộc Device
	eGroup.GET("/api/core/v1/device/list_bucket", rtdb.RetrieveListBucket)
	// API Tạo mới một Device
	eGroup.POST("/api/core/v1/device/create/:device", rtdb.CreateDevice)
	// API Xoá một Record trong Bucket
	eGroup.DELETE("/api/core/v1/device/delete/:device", rtdb.DeleteDevice)
	// API Cập nhật một Device trong Bucket
	eGroup.PATCH("/api/core/v1/device/update/:device", rtdb.UpdateDevice)

	/****************************** BUCKET API *****************************/
	// API Lấy thông tin một Bucket cụ thể
	gValidateAPIKey.GET("/api/core/v1/bucket/detail/*", rtdb.RetrieveBucket)
	// API Lấy toàn bộ thông tin các Buckets
	eGroup.GET("/api/core/v1/bucket/all", rtdb.RetrieveAllBucket)
	// API Lấy toàn bộ thông tin các Buckets trong Device
	gValidateAPIKey.GET("/api/core/v1/bucket/all_in_device", rtdb.RetrieveAllBucketInDevice)
	// API Lấy danh sách và số lượng Bucket thuộc Device
	gValidateAPIKey.GET("/api/core/v1/bucket/list", rtdb.RetrieveNumberBuckets)
	// API lấy về danh sách Bucket thuộc Device
	gValidateAPIKey.GET("/api/core/v1/bucket/list_record/:bucket", rtdb.RetrieveListRecord)
	// API Tạo mới một Bucket
	gValidateAPIKey.POST("/api/core/v1/bucket/create/:bucket", rtdb.CreateBucket)
	// API Xoá một Bucket
	gValidateAPIKey.DELETE("/api/core/v1/bucket/delete/:bucket", rtdb.DeleteBucket)
	// API Cập nhật thông tin một Bucket
	gValidateAPIKey.PATCH("/api/core/v1/bucket/update/:bucket", rtdb.UpdateBucket)

	/****************************** DATA API *****************************/
	// API Lấy một Record cụ thể trong Bucket
	gValidateAPIKey.GET("/api/core/v1/data/detail/:bucket/*", recordMgnt.RetrieveData, statistic.MonitorAPICall)
	// API Lấy toàn bộ Record trong Bucket
	gValidateAPIKey.GET("/api/core/v1/data/all_in_bucket/:bucket", recordMgnt.RetrieveAllDataInBucket, statistic.MonitorAPICall)
	// API Lấy toàn bộ thông tin của 1 Field cụ thể trong tất cả Record của Bucket
	gValidateAPIKey.GET("/api/core/v1/data/all_in_bucket/detailField/:bucket", recordMgnt.GetFieldDetailInAllRecord, statistic.MonitorAPICall)
	// API Lấy toàn bộ Record của User
	eGroup.GET("/api/core/v1/data/all", recordMgnt.RetrieveAllData, mdw.ValidateRateLimit, statistic.MonitorAPICall)
	// API Lấy danh sách và số lượng Records trong Bucket
	gValidateAPIKey.GET("/api/core/v1/data/list/:bucket", recordMgnt.RetrieveNumberRecords, statistic.MonitorAPICall)
	// API Lấy về tổng số lượng Records thuộc User
	eGroup.GET("/api/core/v1/data/total", recordMgnt.RetrieveTotalRecords, statistic.MonitorAPICall)
	// API Tìm kiếm các gía trị của Data theo Field:Value trong DB
	gValidateAPIKey.GET("/api/core/v1/data/all_in_bucket/search/:bucket", recordMgnt.SearchData)
	// API Sort dữ liệu trả về theo Field trong bucket
	gValidateAPIKey.GET("/api/core/v1/data/all_in_bucket/sort/:bucket", recordMgnt.SortData)
	// API Limit dữ liệu trả về theo Field (có thể Sort hoặc không) trong bucket
	gValidateAPIKey.GET("/api/core/v1/data/all_in_bucket/limit/:bucket", recordMgnt.LimitData)
	// API Limit dữ liệu trả về theo Pagging & Limit dữ liệu trong bucket
	gValidateAPIKey.GET("/api/core/v1/data/all_in_bucket/pagging/:bucket", recordMgnt.PaginateWithSkip)
	// API Query toàn bộ Data theo các điều kiện matching với Field:Value trong DB trong bucket
	gValidateAPIKey.POST("/api/core/v1/data/all_in_bucket/match/:bucket", recordMgnt.MatchData)
	// API Tạo một Record mới trong Bucket, Thành công nếu Record là chưa tồn tại
	gValidateAPIKey.POST("/api/core/v1/data/create/:bucket/:record", recordMgnt.SavingData, statistic.MonitorAPICall)
	// API Nhận về một vài record theo yêu cầu từ Body Request
	gValidateAPIKey.POST("/api/core/v1/data/retrieve_many/:bucket", recordMgnt.RetrieveManyData, statistic.MonitorAPICall)
	// API Xoá một Record trong Bucket
	gValidateAPIKey.DELETE("/api/core/v1/data/delete/:bucket/*", recordMgnt.DeleteData, statistic.MonitorAPICall)
	// API Cập nhật một Field của một Record trong Bucket
	gValidateAPIKey.PATCH("/api/core/v1/data/update_one/:bucket/:record/*", recordMgnt.UpdateOne, statistic.MonitorAPICall)
	// API Cập nhật toàn bộ nội dung của Record trong Bucket
	gValidateAPIKey.PATCH("/api/core/v1/update_many/:bucket/:record", recordMgnt.UpdateMany, statistic.MonitorAPICall)
	// API Cập nhật vài field cụ thể có tồn tại của Record trong Bucket
	gValidateAPIKey.PATCH("/api/core/v1/data/update_field/:bucket/:record", recordMgnt.UpdateSomeField, statistic.MonitorAPICall)

	gValidateAPIKey.POST("/api/core/v1/data/relation/add/:bucket/:record", recordMgnt.AddRelationship)
	gValidateAPIKey.POST("/api/core/v1/data/relation/remove/:bucket/:record", recordMgnt.RemoveRelationship)

	/****************************** MEDIA API *****************************/
	// SWIFT STORAGE: API Upload một Media File lên Swift Storage và lưu thông tin của file đã được upload vào DB
	gValidateAPIKey.POST("/api/core/v1/swift/upload/:bucket", rtdb.SwiftUploadFile, statistic.MonitorAPICall)
	// SWIFT STORAGE: API Xoá một Media File Record trong Bucket
	gValidateAPIKey.DELETE("/api/core/v1/swift/delete/:bucket/:record", rtdb.SwiftDeleteFile, statistic.MonitorAPICall)

	// MINIO STORAGE: API lấy toàn bộ object storage file thuộc user
	eGroup.GET("/api/core/v1/minio/object/all", rtdb.MinioRetrieveAllObjects, statistic.MonitorAPICall)
	// MINIO STORAGE: API lấy toàn bộ object storage file thuộc user
	eGroup.GET("/api/core/v1/minio/bucket/all", rtdb.MinioRetrieveAllBucket, statistic.MonitorAPICall)
	// MINIO STORAGE: API Lấy danh sách và số lượng Minio Bucket thuộc Device
	gValidateAPIKey.GET("/api/core/v1/minio/bucket/list", rtdb.MinioRetrieveNumberBuckets)
	// MINIO STORAGE: API tạo một bucket Minio mới
	gValidateAPIKey.POST("/api/core/v1/minio/create/:bucket", rtdb.MinioCreateBucket, statistic.MonitorAPICall)
	// MINIO STORAGE: API Upload một Media File lên Minio Storage và lưu thông tin của file đã được upload vào DB
	gValidateAPIKey.POST("/api/core/v1/minio/upload/:bucket", rtdb.MinioUploadFile, statistic.MonitorAPICall)
	// MINIO STORAGE: API xoá một bucket Minio
	gValidateAPIKey.DELETE("/api/core/v1/minio/delete_bucket/:bucket", rtdb.MinioDeleteBucket, statistic.MonitorAPICall)
	// MINIO STORAGE: API Xoá một Media File Record trong Bucket
	gValidateAPIKey.DELETE("/api/core/v1/minio/delete_object/:bucket/:record", rtdb.MinioDeleteFile, statistic.MonitorAPICall)

	/****************************** QUEUE API *****************************/
	gValidateAPIKey.POST("/api/core/v1/queue/push/:queue", queue.PushDataToQueue)
	gValidateAPIKey.POST("/api/core/v1/queue/push_by_datetime/:queue", queue.PushDataByDateTimeToQueue)
	gValidateAPIKey.GET("/api/core/v1/queue/list/:queue", queue.ListDataInQueue)
	gValidateAPIKey.GET("/api/core/v1/queue/pop/:queue", queue.PopDataFromQueue)
	gValidateAPIKey.GET("/api/core/v1/queue/sort_by_datetime/:queue", queue.SortDataByDateTimeInQueue)

	/****************************** VNPT e-Token API *****************************/
	// API lấy về thông tin ứng dụng đăng ký sử dụng dịch vụ VNPT eToken theo
	eGroup.GET("/api/core/v1/etoken/code/:app_id", authentication.GetLinkCodeEToken)
	// API lấy về toàn bộ danh sách ứng dụng đã đăng ký sử dụng VNPT eToken
	eGroup.GET("/api/core/v1/etoken/code/all", authentication.GetAllLinkCodeEToken)
	// API liên kết ứng dụng sử dụng eToken trên Mobile App
	eGroup.POST("/api/core/v1/etoken/device/registry", authentication.RegistryServiceUseEToken)
	// API huỷ liên kết ứng dụng sử dụng eToken trên Mobile App
	eGroup.POST("/api/core/v1/etoken/device/unregistry", authentication.UnRegistryerviceUseEToken)
	// API Validate ứng dụng sử dụng eToken và Mobile App eToken đã liên kết chưa
	eGroup.POST("/api/core/v1/etoken/device/validate", authentication.ValidateServiceRegistriedEToken)
	// API thực hiện Generate ra 1 VNPT eToken có hiệu lực trong 1 phút
	eGroup.POST("/api/core/v1/etoken/generate", authentication.GenerateETokenData)
	// API thực hiện Validate 1 VNPT eToken còn hiệu lực hay không
	eGroup.POST("/api/core/v1/etoken/validate", authentication.ValidateETokenData)
	// API đăng ký dịch vụ sử dụng VNPT eToken và Generate ra 1 mã liên kết cho 3rd Application
	eGroup.POST("/api/core/v1/etoken/code/generate", authentication.GenerateLinkCodeEToken)
	// API huỷ đăng ký dịch vụ sử dụng VNPT eToken
	eGroup.DELETE("/api/core/v1/etoken/code/:app_id", authentication.DeleteLinkCodeEToken)

	go terminateProcess()

	e.Logger.Fatal(e.StartServer(s))
}

func terminateProcess() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	for {
		<-c
		mgodb.Close()
		db.Close()
		fmt.Println("\r- Ctrl+C pressed in Terminal. Close All Redis Connection.")
		os.Exit(0)
	}
}
