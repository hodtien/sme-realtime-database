package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/model"
	utils "bitbucket.org/cloud-platform/sme-realtime-database/utils"
	"github.com/labstack/echo"
)

/*========================== BUCKET MANAGEMENT ==========================*/

// RetrieveNumberBuckets - Nhận số lượng Bucket của User
func RetrieveNumberBuckets(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	//bucket := c.Param("bucket")
	redisKey := "BM" + "$" + userID + "$" + deviceID
	keys := db.Keys(redisKey)
	var keysInterface []interface{}
	for _, k := range keys {
		k = strings.ReplaceAll(k, "BM"+"$"+userID+"$"+deviceID+"$", "")
		keysInterface = append(keysInterface, k)
	}
	retData := map[string]interface{}{
		"number": len(keys),
		"list":   keysInterface,
	}
	return c.JSON(200, retData)
}

//RetrieveAllBucket - Lấy toàn bộ danh sách Bucket
func RetrieveAllBucket(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}

	redisKey := "BM" + "$" + userID + "$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	ret, err := db.ReJSONGetAll(redisKey, ".", keys, "bucket_all")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	return c.JSON(200, ret)
}

//RetrieveAllBucketInDevice - Lấy toàn bộ danh sách Bucket thuộc 1 device
func RetrieveAllBucketInDevice(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	//bucket := c.Param("bucket")
	redisKey := "BM" + "$" + userID + "$" + deviceID + "$" //+ bucket + "$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(200, map[string]interface{}{"code": "0", "data": nil})
	}
	ret, err := db.ReJSONGetAll(redisKey, ".", keys, "bucket", deviceID)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "data": nil})
	}
	return c.JSON(200, ret)
}

//RetrieveBucket - Nhận một Thông tin Bucket
func RetrieveBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	uri := c.Request().URL.Path
	uri = strings.ReplaceAll(uri, "/api/core/v1/bucket/detail/", "")
	//key, query := middleware.URIToQuery(uri)
	uriArr := strings.Split(uri, "/")
	uriArr = middleware.DeleteEmpty(uriArr)
	bucket := uriArr[0]
	redisKey := "BM" + "$" + userID + "$" + deviceID + "$" + bucket
	uriArr = uriArr[1:]
	query := ""
	for _, v := range uriArr {
		if middleware.IsNumber(v) {
			query = query + "[" + v + "]"
		} else {
			query = query + "." + v
		}
	}
	if query == "" {
		query = "."
	}
	ret, err := db.ReJSONGet(redisKey, query)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	respData := map[string]interface{}{
		"record_id": bucket,
		"data_item": ret,
	}
	return c.JSON(200, respData)
}

//CreateBucket - Tạo 1 bucket, thất bại nếu bản ghi đã tồn tại - Error Code 21
func CreateBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucketID := c.Param("bucket")
	redisKey := "BM" + "$" + userID + "$" + deviceID + "$" + bucketID

	b := new(model.Bucket)
	if err := c.Bind(b); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Bucket Parameter is Invalid"})
	}
	//b.ListRecord = nil
	b.ID = bucketID
	if err := c.Validate(b); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	if !middleware.IsStringAlphabetic(b.ID) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	bodyBytes, err := json.Marshal(b)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	var args interface{} = "NX"

	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), args)
	if err != nil || ret != "OK" {
		fmt.Println("SET DATA BUCKET: ", err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	// Sync to Persistent Database (MongoDB)
	data := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}
	if err = AddToListBucketController("DM"+"$"+userID+"$"+deviceID, bucketID); err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		data["id"] = redisKey
		collection := "DM" + "@" + userID + "@" + deviceID
		go utils.KafkaPublishSaveOrUpdate(kafkaProducerBucketSOU, apikey, collection, redisKey, string(bodyBytes))
	}

	return c.JSON(201, map[string]interface{}{"code": "0", "message": "OK"})
}

//DeleteBucket - Xoá một Bucket - Error Code 22
func DeleteBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	redisKey := userID + "$" + deviceID + "$" + bucket

	DeleteBucketController(redisKey)
	if err := RemoveFromListBucketController("DM"+"$"+userID+"$"+deviceID, bucket); err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			utils.KafkaPublishDrop(kafkaProducerDrop, apikey, "Drop", "BM"+"@"+userID+"@"+deviceID+"@"+bucket)
			utils.KafkaPublishDelete(kafkaProducerDelete, apikey, "DM"+"@"+userID+"@"+deviceID, "BM"+"$"+redisKey)
		}()
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

//UpdateBucket - Update lại toàn bộ nội dung của 1 Bucket - Error Code 23
func UpdateBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")

	redisKey := "BM" + "$" + userID + "$" + deviceID + "$" + bucket
	// Read the Body content
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		defer c.Request().Body.Close()
	}
	// Restore the io.ReadCloser to its original state
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	bucketNew := new(model.Bucket)
	err = json.Unmarshal(bodyBytes, bucketNew)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Failed"})
	}

	if bucketNew.ID != "" && !middleware.IsStringAlphabetic(bucketNew.ID) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	ret, err := db.ReJSONGet(redisKey, ".")
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Failed"})
	}

	bucketOld, _ := ret.(map[string]interface{})

	bucketNew.ID = fmt.Sprintf("%v", bucketOld["id"])
	bucketNew.CreatedByDevice = fmt.Sprintf("%v", bucketOld["created_by_device"])
	bucketNew.CreatedByUser = fmt.Sprintf("%v", bucketOld["created_by_user"])

	bodyBytes, err = json.Marshal(bucketNew)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Failed"})
	}

	ret, err = db.ReJSONSet(redisKey, ".", string(bodyBytes), nil)
	if err != nil || ret != "OK" {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		collection := "DM" + "@" + userID + "@" + deviceID
		go utils.KafkaPublishSaveOrUpdate(kafkaProducerBucketSOU, apikey, collection, redisKey, string(bodyBytes))
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

//RetrieveListRecord - Nhận về danh sách Record thuộc Bucket
func RetrieveListRecord(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")

	redisKey := "BM" + "$" + userID + "$" + deviceID + "$" + bucket
	ret, err := GetListRecordController(redisKey)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	return c.JSON(200, ret)
}
