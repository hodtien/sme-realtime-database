/*
* VNPT Business Platform:
* Xử lý nghiệp vụ quản lý các thiết bị (Mobile, Webapp) đăng ký và xác thực với hệ thống VNPT Platform.
* Sau khi đăng ký và xác thực, các thiết bị này mới có thể sử dụng
* dịch vụ Backend as a Service và Storage as a Service của VNPT Business Platform.
 */

package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/model"
	utils "bitbucket.org/cloud-platform/sme-realtime-database/utils"
	"github.com/labstack/echo"
)

/*========================== DEVICE MANAGEMENT ==========================*/

// // Validate - Validate HTTP Data Input
// func (cv *CustomValidator) Validate(i interface{}) error {
// 	return cv.Validator.Struct(i)
// }

// CreateDevice - Lưu một Device mới được đăng ký sử dụng API vào DB, thất bại nếu bản ghi đã tồn tại
func CreateDevice(c echo.Context) error {
	deviceID := c.Param("device")
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	//bucket := "_device_"
	//redisKey := userID + bucket + record
	redisKey := "DM" + "$" + userID + "$" + deviceID

	d := new(model.Device)
	if err := c.Bind(d); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if err := c.Validate(d); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	if !middleware.IsStringAlphabetic(d.ID) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	// Add User ID and Device ID to JWT Token.
	APIKey, err := middleware.GenerateDeviceAPIKey(userID, deviceID)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	d.APIKey = APIKey
	//d.ListBucket = nil
	d.ID = deviceID

	bodyBytes, err := json.Marshal(d)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Device Failed"})
	}

	var args interface{} = "NX"
	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), args)
	if err != nil || ret != "OK" {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Device Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		collection := userID
		go utils.KafkaPublishSaveOrUpdate(kafkaProducerDeviceSOU, "", collection, redisKey, string(bodyBytes))
	}

	return c.JSON(201, map[string]interface{}{"code": "0", "message": "OK"})
}

// DeleteDevice - Xoá một Device đã tồn tại trong DB
func DeleteDevice(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	deviceID := c.Param("device")
	redisKey := "DM" + "$" + userID + "$" + deviceID

	// Lấy về API Key của Device tương ứng
	ret, err := db.ReJSONGet("DM"+"$"+userID+"$"+deviceID, ".apiKey")
	if err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Token is Invalid"})
	}
	deviceAPIKey := fmt.Sprintf("%s", ret)

	// Xoá API Key của Device tương ứng trước khi xoá Device khỏi hệ thống
	if !middleware.DestroyJWT(deviceAPIKey) {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Delete API Key Device is Failed"})
	}

	// Xoá Device
	err = db.Delete(redisKey)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Not found"})
	}

	// Xoá toàn bộ Bucket và Record thuộc về Device
	DeleteDeviceController(userID + "$" + deviceID)

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			listBucket := mgodb.FindAllInMongo("DM" + "@" + userID + "@" + deviceID)
			listBucketStr := ""
			for _, b := range listBucket {
				listBucketStr = listBucketStr + b + ","
			}
			utils.KafkaPublishDrop(kafkaProducerDrop, "", "DropMany", listBucketStr)
			utils.KafkaPublishDrop(kafkaProducerDrop, "", "Drop", "DM"+"@"+userID+"@"+deviceID)
			utils.KafkaPublishDelete(kafkaProducerDelete, "", userID, redisKey)
		}()
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// RetrieveNumberDevices - Nhận số lượng Device đang đã đăng ký
func RetrieveNumberDevices(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	//bucket := "_device"
	//redisKey := userID + bucket
	redisKey := "DM" + "$" + userID
	keys := db.Keys(redisKey)
	var keysInterface []interface{}
	for _, k := range keys {
		k = strings.ReplaceAll(k, "DM"+"$"+userID+"$", "")
		keysInterface = append(keysInterface, k)
	}
	retData := map[string]interface{}{
		"number": len(keys),
		"list":   keysInterface,
	}
	return c.JSON(200, retData)
}

// RetrieveDevice - Nhận một Device trong Bucket
func RetrieveDevice(c echo.Context) error {
	uri := c.Request().URL.Path
	uri = strings.ReplaceAll(uri, "/api/core/v1/device/detail/", "")
	uriArr := strings.Split(uri, "/")
	uriArr = middleware.DeleteEmpty(uriArr)
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	//bucket := "device"
	deviceID := uriArr[0]

	redisKey := "DM" + "$" + userID + "$" + deviceID
	query := ""
	if len(uriArr) > 1 {
		uriArr = uriArr[1:]
		for _, v := range uriArr {
			if middleware.IsNumber(v) {
				query = query + "[" + v + "]"
			} else {
				query = query + "." + v
			}
		}
	} else {
		query = "."
	}

	ret, err := db.ReJSONGet(redisKey, query)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	respData := map[string]interface{}{
		"device_id": deviceID,
		"data_item": ret,
	}
	return c.JSON(200, respData)
}

//RetrieveAllDevice - Lấy toàn bộ Device của Bucket
func RetrieveAllDevice(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	// bucket := "_device"
	// redisKey := userID + bucket
	redisKey := "DM" + "$" + userID + "$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	ret, err := db.ReJSONGetAll(redisKey, ".", keys, "device")
	if err != nil || ret == nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Not found"})
	}
	return c.JSON(200, ret)
}

// UpdateDevice - Update thông tin 1 Device trong Bucket
func UpdateDevice(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	deviceID := c.Param("device")
	redisKey := "DM" + "$" + userID + "$" + deviceID

	// Read the Body content
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		defer c.Request().Body.Close()
	}
	// Restore the io.ReadCloser to its original state
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	d := new(model.Device)
	err := json.Unmarshal(bodyBytes, d)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Device Failed"})
	}

	ret, err := db.ReJSONGet(redisKey, ".apiKey")
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Device Failed"})
	}
	d.APIKey = fmt.Sprintf("%s", ret)

	ret, err = db.ReJSONGet(redisKey, ".id")
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Device Failed"})
	}
	d.ID = fmt.Sprintf("%s", ret)

	bodyBytes, err = json.Marshal(d)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Device Failed"})
	}

	ret, err = db.ReJSONSet(redisKey, ".", string(bodyBytes), nil)
	if err != nil || ret != "OK" {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Device Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		collection := userID
		go utils.KafkaPublishSaveOrUpdate(kafkaProducerDeviceSOU, "", collection, redisKey, string(bodyBytes))
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// SearchDevice - Tìm kiếm một Device được lưu trong Persistent MongoDB
func SearchDevice(c echo.Context) error {
	field := c.QueryParam("field")
	pattern := c.QueryParam("pattern")
	retSearch, err := mgodb.SearchInMongo("device", field, pattern)
	if !err {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Device Not Found"})
	}
	return c.JSON(200, retSearch)
}

//RetrieveListBucket - Nhận về danh sách Bucket thuộc Device
func RetrieveListBucket(c echo.Context) error {

	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	redisKey := "DM" + "$" + userID + "$" + deviceID
	ret, err := GetListBucketController(redisKey)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Not Found"})
	}
	return c.JSON(200, ret)
}
