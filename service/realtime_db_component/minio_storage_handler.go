package service

import (
	"encoding/json"
	"fmt"
	"log"
	"mime/multipart"
	"strings"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/model"
	utils "bitbucket.org/cloud-platform/sme-realtime-database/utils"
	"github.com/labstack/echo"
	"github.com/minio/minio-go"
)

// MinioCreateBucket - Create new Minio Bucket - Error Code 27
// MinioCreateBucket godoc
// @Summary Tạo mới một Minio Storage Bucket
// @Description Tạo mới một Minio Storage Bucket
// @Router /api/core/v1/minio/create/:bucket [post]
func MinioCreateBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")

	// Initialize minio client object.
	minioClient, err := minio.New(minioEndpoint, minioAccessKeyID, minioSecretAccessKey, minioUseSSL)
	if err != nil {
		fmt.Println(err)
	}
	exists, err := minioClient.BucketExists(bucket)
	if err != nil || exists {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}
	// Make a new bucket if bucket is not exist.
	if err := minioClient.MakeBucket(bucket, minioLocation); err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	// Create Bucket in Redis DB
	redisKey := "minio" + "$" + "BM" + "$" + userID + "$" + deviceID + "$" + bucket
	b := new(model.Bucket)
	if err := c.Bind(b); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	b.ID = bucket
	if err := c.Validate(b); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	bodyBytes, err := json.Marshal(b)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	var args interface{} = "NX"

	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), args)
	if err != nil || ret != "OK" {
		fmt.Println("SET DATA BUCKET: ", err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	// Sync to Persistent Database (MongoDB)
	data := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}
	if err = AddToListBucketController("DM"+"$"+userID+"$"+deviceID, bucket); err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Bucket Failed"})
	}

	go func() {
		data["id"] = redisKey
		//db.SaveMongo("list_bucket", redisKey, data)
		mgodb.SaveMongo(MongoHost, MgoDBName, "DM"+"$"+userID+"$"+deviceID, redisKey, data)
		fmt.Println("Persistent Bucket to MongoDB")
		utils.HTTPSynchronizeRTDB(apikey, []byte(errCode27))
	}()
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// MinioDeleteBucket - Delete a Bucket exists - Error Code 29
// MinioDeleteBucket godoc
// @Summary Xoá một Minio Storage Bucket
// @Description Xoá một Minio Storage Bucket
// @Router /api/core/v1/minio/delete_bucket/:bucket [delete]
func MinioDeleteBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")
	redisKey := "minio" + "$" + "BM" + "$" + userID + "$" + deviceID + "$" + bucket
	// Initialize minio client object.
	minioClient, err := minio.New(minioEndpoint, minioAccessKeyID, minioSecretAccessKey, minioUseSSL)
	if err != nil {
		fmt.Println(err)
	}
	exists, err := minioClient.BucketExists(bucket)
	if !db.Exists(redisKey) {
		if err != nil || !exists {
			fmt.Println(err)
			return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Remove Bucket Failed"})
		}
	} else {
		err = db.Delete(redisKey)
		if err != nil {
			return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Remove Bucket Failed"})
		}
		return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
	}

	// Remove all Object in Bucket
	// Create a done channel to control 'ListObjectsV2' go routine.
	doneCh := make(chan struct{})
	// Indicate to our routine to exit cleanly upon return.
	defer close(doneCh)
	isRecursive := true
	objectCh := minioClient.ListObjectsV2(bucket, "", isRecursive, doneCh)
	for object := range objectCh {
		if object.Err != nil {
			fmt.Println(object.Err)
			return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Delete Multimedia File Failed"})
		}
		redisKey := "minio" + "$" + userID + "$" + deviceID + "$" + bucket + "$" + object.Key
		if err = db.Delete(redisKey); err != nil {
			return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Delete Multimedia File Failed"})
		}
		if err := minioClient.RemoveObject(bucket, object.Key); err != nil {
			fmt.Println(err)
			return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Delete Multimedia File Failed"})
		}
	}

	if err := db.Delete(redisKey); err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Remove Bucket Failed"})
	}
	if err := minioClient.RemoveBucket(bucket); err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Remove Bucket Failed"})
	}
	go func() {
		utils.HTTPSynchronizeRTDB(apikey, []byte(errCode29))
	}()
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// MinioUploadFile - Minio Upload Media File - Error Code 28
// MinioCreateBucket godoc
// @Summary Upload File tới một Bucket trên Minio Storage
// @Description Upload File tới một Bucket trên Minio Storage
// @Router /api/core/v1/minio/upload/:bucket [post]
func MinioUploadFile(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")

	// Initialize minio client object.
	minioClient, err := minio.New(minioEndpoint, minioAccessKeyID, minioSecretAccessKey, minioUseSSL)
	if err != nil {
		fmt.Println(err)
	}
	exists, err := minioClient.BucketExists(bucket)
	if err != nil || !exists {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Bucket is not exists"})
	}

	// Lấy form data
	form, err := c.MultipartForm()
	if err != nil {
		log.Println("Failed to get form data, detail error:", err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Upload failed"})
	}

	// Lấy danh sách file trong form với key là "data"
	files := form.File["data"]
	if files == nil {
		log.Println("No files need to upload")
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "No data available"})
	}

	fileName := ""
	contentType := c.Request().Header.Get("Content-type")
	var infoMediaUploadArr []interface{}
	for _, file := range files {
		currentTime := time.Now().Local()
		timeUpload := currentTime.Format("02-01-2006 15-03-04")

		// Lấy nội dung file
		src, err := file.Open()
		if err != nil {
			log.Println("Failed to open file, detail error:", err)
			c.JSON(500, map[string]interface{}{"code": "-1", "message": "Data is unavailable"})
		}
		//defer src.Close()
		size := file.Size

		fileName = file.Filename
		var opts minio.PutObjectOptions
		opts.ContentType = strings.Split(contentType, ";")[0]
		// Upload the file with PutObject
		go func(src multipart.File, bucket, fileName string, size int64, opts minio.PutObjectOptions) {
			n, err := minioClient.PutObject(bucket, fileName, src, size, opts)
			defer src.Close()
			if err != nil {
				fmt.Println(err)
			}
			fmt.Printf("Successfully uploaded %s of size %d\n", fileName, n)
		}(src, bucket, fileName, size, opts)
		infoMediaUpload := map[string]interface{}{
			"id":           fileName,
			"content_type": strings.Split(contentType, ";")[0],
			"path":         "http://203.162.141.37:9000/" + bucket + "/" + fileName,
			"upload_time":  timeUpload,
		}
		bodyBytes, err := json.Marshal(infoMediaUpload)
		if err != nil {
			return c.JSON(200, map[string]interface{}{"code": "1", "message": "Upload Success But Response Message Invalid"})
		}

		// Lưu thông tin File đã được Upload vào RedisDB
		record := fileName
		redisKey := "minio" + "$" + userID + "$" + deviceID + "$" + bucket + "$" + record
		ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), nil)
		if err != nil || ret != "OK" {
			return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Upload Success But Save Information Failed"})
		}
		infoMediaUploadArr = append(infoMediaUploadArr, infoMediaUpload)
	}
	go func() {
		utils.HTTPSynchronizeRTDB(apikey, []byte(errCode28))
	}()
	return c.JSON(200, infoMediaUploadArr)
}

// MinioDeleteFile - Minio Delete Media File - Error Code 30
// MinioDeleteFile godoc
// @Summary Xoá một File thuộc một Bucket trên Minio Storage
// @Description Xoá một File thuộc một Bucket trên Minio Storage
// @Router /api/core/v1/minio/delete_object/:bucket/:record [delete]
func MinioDeleteFile(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")
	objectName := c.Param("record")

	// Initialize minio client object.
	minioClient, err := minio.New(minioEndpoint, minioAccessKeyID, minioSecretAccessKey, minioUseSSL)
	if err != nil {
		fmt.Println(err)
	}
	exists, err := minioClient.BucketExists(bucket)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Remove Object Failed"})
	}
	if !exists {
		return c.JSON(200, "Bucket "+bucket+" is not Exist ")
	}

	redisKey := "minio" + "$" + userID + "$" + deviceID + "$" + bucket + "$" + objectName
	err = db.Delete(redisKey)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Delete Multimedia File Failed"})
	}
	if err := minioClient.RemoveObject(bucket, objectName); err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Remove Object Failed"})
	}

	go func() {
		utils.HTTPSynchronizeRTDB(apikey, []byte(errCode30))
	}()

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// MinioRetrieveAllObjects - Minio List all Media File
// MinioRetrieveAllObjects godoc
// @Summary Lấy về danh sách File thuộc một User trên Minio Storage
// @Description Xoá một File thuộc một User trên Minio Storage
// @Router /api/core/v1/minio/object/all [get]
func MinioRetrieveAllObjects(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	redisKey := "minio" + "$" + userID + "$"

	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}

	ret, err := db.ReJSONGetAll(redisKey, ".", keys, "minio_record_all")
	if err != nil || ret == nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Not found"})
	}
	return c.JSON(200, ret)
}

// MinioRetrieveAllBucket - Minio List all Bucket Media File
// MinioRetrieveAllBucket godoc
// @Summary Lấy về danh sách File thuộc một User trên Minio Storage
// @Description Xoá một File thuộc một User trên Minio Storage
// @Router /api/core/v1/minio/bucket/all [get]
func MinioRetrieveAllBucket(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}

	redisKey := "minio" + "$" + "BM" + "$" + userID + "$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	ret, err := db.ReJSONGetAll(redisKey, ".", keys, "minio_bucket_all")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	return c.JSON(200, ret)
}

// MinioRetrieveNumberBuckets - Nhận số lượng Minio Bucket của User
func MinioRetrieveNumberBuckets(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	//bucket := c.Param("bucket")
	redisKey := "minio" + "$" + "BM" + "$" + userID + "$" + deviceID
	keys := db.Keys(redisKey)
	var keysInterface []interface{}
	for _, k := range keys {
		k = strings.ReplaceAll(k, "minio"+"$"+"BM"+"$"+userID+"$"+deviceID+"$", "")
		keysInterface = append(keysInterface, k)
	}
	retData := map[string]interface{}{
		"number": len(keys),
		"list":   keysInterface,
	}
	return c.JSON(200, retData)
}
