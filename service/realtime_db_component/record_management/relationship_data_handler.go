package service

import (
	"fmt"
	"strings"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/model"
	"github.com/labstack/echo"
)

// AddRelationship - AddRelationship
func AddRelationship(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	record := c.Param("record")

	dataRelationship := new(model.DataRelationship)
	if err := c.Bind(dataRelationship); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Parameter is Invalid"})
	}

	if err := c.Validate(dataRelationship); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	parentBucketID := "BM" + "$" + userID + "$" + deviceID + "$" + dataRelationship.ParentBucketID
	parentRecordID := userID + "$" + deviceID + "$" + dataRelationship.ParentBucketID + "$" + dataRelationship.ParentRecordID
	if !db.Exists(parentBucketID) || !db.Exists(parentRecordID) {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Bucket Or Record is not exists"})
	}
	childRecordID := userID + "$" + deviceID + "$" + bucket + "$" + record
	redisKey := "Relationship$" + parentRecordID
	if _, err := db.LPush(redisKey, childRecordID); err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "FAILED"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "SUCCESS"})
}

// RemoveRelationship - RemoveRelationship
func RemoveRelationship(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	record := c.Param("record")
	dataRelationship := new(model.DataRelationship)
	if err := c.Bind(dataRelationship); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Parameter is Invalid"})
	}

	if err := c.Validate(dataRelationship); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	parentBucketID := "BM" + "$" + userID + "$" + deviceID + "$" + dataRelationship.ParentBucketID
	parentRecordID := userID + "$" + deviceID + "$" + dataRelationship.ParentBucketID + "$" + dataRelationship.ParentRecordID
	if !db.Exists(parentBucketID) || !db.Exists(parentRecordID) {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Bucket Or Record is not exists"})
	}
	childRecordID := userID + "$" + deviceID + "$" + bucket + "$" + record
	redisKey := "Relationship$" + parentRecordID
	if _, err := db.LRemove(redisKey, childRecordID); err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "FAILED"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "SUCCESS"})
}
