package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"github.com/labstack/echo"
)

/*========================== DATA MANAGEMENT ==========================*/

// RetrieveNumberRecords - Nhận số lượng Records đang có trong bucket
func RetrieveNumberRecords(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")
	redisKey := userID + "$" + deviceID + "$" + bucket
	keys := db.Keys(redisKey)
	var keysInterface []interface{}
	for _, k := range keys {
		k = strings.ReplaceAll(k, userID+"$"+deviceID+"$"+bucket+"$", "")
		keysInterface = append(keysInterface, k)
	}
	retData := map[string]interface{}{
		"number": len(keys),
		"list":   keysInterface,
	}
	return c.JSON(200, retData)
}

// RetrieveTotalRecords - Nhận số lượng Records đang có
func RetrieveTotalRecords(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	redisKey := "all$" + userID
	totalMembers, err := db.ZCard(1, redisKey)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "FAILED"})
	}

	retData := map[string]interface{}{
		"number": totalMembers,
	}
	return c.JSON(200, retData)
}

// RetrieveData - Nhận một Record trong Bucket
func RetrieveData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	uri := c.Request().URL.Path
	uri = strings.ReplaceAll(uri, "/api/core/v1/data/detail/", "")
	//key, query := middleware.URIToQuery(uri)
	uriArr := strings.Split(uri, "/")
	uriArr = middleware.DeleteEmpty(uriArr)
	bucket := uriArr[0]
	record := uriArr[1]
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
	uriArr = uriArr[2:]
	query := ""
	for _, v := range uriArr {
		if middleware.IsNumber(v) {
			query = query + "[" + v + "]"
		} else {
			query = query + "." + v
		}
	}
	if query == "" {
		query = "."
	}

	ret, err := db.ReJSONGet(redisKey, query)
	if err != nil {
		//checkAndInsert(userID, deviceID, bucket, record)
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	respData := map[string]interface{}{
		"record_id": record,
		"data_item": ret,
	}
	return c.JSON(200, respData)
}

// RetrieveAllDataInBucket - Lấy toàn bộ Records trong 1 Bucket
func RetrieveAllDataInBucket(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket, query := "", ""
	uri := c.Request().URL.Path
	uri = strings.Trim(uri, "")
	uri = strings.ReplaceAll(uri, "/api/core/v1/data/all_in_bucket/", "")
	if uri != "" && strings.Contains(uri, "/") {
		uriArr := strings.Split(uri, "/")
		bucket = uriArr[0]
		uriArr = middleware.DeleteEmpty(uriArr)
		uriArr = uriArr[1:]
		for _, v := range uriArr {
			if middleware.IsNumber(v) {
				query = query + "[" + v + "]"
			} else {
				query = query + "." + v
			}
		}
	} else {
		bucket = uri
		query = "."
	}

	redisKey := userID + "$" + deviceID + "$" + bucket + "$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	ret, err := db.ReJSONGetAllInBucket(redisKey, query, keys, "record", deviceID, bucket)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	return c.JSON(200, ret)
}

// RetrieveAllData - Lấy toàn bộ Records của User
func RetrieveAllData(c echo.Context) (err error) {

	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, "UserID or DeviceID is Invalid")
	}
	var start, stop int64
	if c.QueryParam("start") == "" || c.QueryParam("stop") == "" {
		return c.JSON(400, "Param is invalid")
	}
	start, err = strconv.ParseInt(c.QueryParam("start"), 10, 64)
	if err != nil {
		return c.JSON(400, "Param is invalid")
	}
	stop, err = strconv.ParseInt(c.QueryParam("stop"), 10, 64)
	start = start - 1
	stop = stop - 1
	if err != nil || start > stop {
		return c.JSON(400, "Param is invalid")
	}

	keys, err := db.GetKeysByIndex("all$"+userID, start, stop)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	if len(keys) == 0 {
		return c.JSON(200, map[string]interface{}{"code": "0", "message": "null"})
	}

	redisKey := userID + "$"

	//ret, err := db.ReJSONGetAll(redisKey, ".", keys, "record_all")
	ret, err := db.HashGetAll(redisKey, ".", keys, "record_all")
	if err != nil || ret == nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}

	return c.JSON(200, ret)
}

// RetrieveManyData - Nhận về một vài record theo yêu cầu từ Body Request
func RetrieveManyData(c echo.Context) error {

	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")

	reqBody := new(RetrieveManyRecordRequest)
	if err := c.Bind(reqBody); err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	var listRecord []string
	redisKey := userID + "$" + deviceID + "$" + bucket
	if strings.Contains(reqBody.List, ",") {
		listRecord = strings.Split(reqBody.List, ",")
		for i, record := range listRecord {
			listRecord[i] = redisKey + "$" + strings.Trim(record, " ")
		}
	} else {
		listRecord = append(listRecord, redisKey+"$"+reqBody.List)
	}
	ret, err := db.ReJSONGetAllInBucket(redisKey, ".", listRecord, "record_all")
	if err != nil || ret == nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}

	return c.JSON(200, ret)
}

//SearchData - Tìm kiếm một Data được lưu trong Persistent MongoDB
func SearchData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")
	field := c.QueryParam("field")
	pattern := c.QueryParam("pattern")
	retSearch, b := mgodb.SearchInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, field, pattern)
	if !b {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
	}
	return c.JSON(200, retSearch)
}

//MatchData - Query All Data được lưu trong Persistent MongoDB theo điều kiện được match
func MatchData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		defer c.Request().Body.Close()
	}
	// Restore the io.ReadCloser to its original state
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	data := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println(err)
	}

	fields := make([]string, 0, len(data))
	values := make([]string, 0, len(data))
	for f, v := range data {
		fields = append(fields, f)
		tmp := fmt.Sprintf("%s", v)
		values = append(values, tmp)
	}

	bucket := c.Param("bucket")
	retSearch, b := mgodb.SearchManyConditionInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, fields, values)
	if !b {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
	}
	return c.JSON(200, retSearch)
}

//SortData - Sort Data theo Field
func SortData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	fieldSort := c.QueryParam("field")
	retSort, err := mgodb.SortInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, fieldSort)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
	}
	return c.JSON(200, retSort)
}

//LimitData - Giới hạn dữ liệu nhận về, có thể sort hoặc không.
func LimitData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Param is invalid"})
	}
	fieldSort := c.QueryParam("field")

	retSort, err := mgodb.LimitSortInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, fieldSort, limit)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
	}
	return c.JSON(200, retSort)
}

// PaginateWithSkip - API lấy dữ liệu theo phân trang
func PaginateWithSkip(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return c.JSON(400, "Param is invalid")
	}
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return c.JSON(400, "Param is invalid")
	}

	retPagging, err := mgodb.PaginateWithSkip("BM"+"@"+userID+"@"+deviceID+"@"+bucket, page, limit)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
	}
	return c.JSON(200, retPagging)
}

// GetFieldDetailInAllRecord - GetFieldDetailInAllRecord
func GetFieldDetailInAllRecord(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	query := c.QueryParam("fieldName")

	redisKey := userID + "$" + deviceID + "$" + bucket + "$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	ret, err := db.ReJSONGetAll(redisKey, query, keys, "record", deviceID, bucket)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "data": ret})
}
