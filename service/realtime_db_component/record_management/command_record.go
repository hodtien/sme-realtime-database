package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/utils"
	"github.com/labstack/echo"
)

// DeleteData - Xoá một bản ghi đã tồn tại trong DB - Error Code 12
func DeleteData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	uri := c.Request().URL.Path
	uri = strings.ReplaceAll(uri, "/api/core/v1/data/delete/", "")
	uriArr := strings.Split(uri, "/")
	uriArr = middleware.DeleteEmpty(uriArr)
	bucket := uriArr[0]
	record := uriArr[1]
	uriArr = uriArr[2:]
	query := ""
	for _, v := range uriArr {
		if middleware.IsNumber(v) {
			query = query + "[" + v + "]"
		} else {
			query = query + "." + v
		}
	}
	if query == "" {
		query = "."
	}
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

	if err = db.Delete(redisKey); err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if _, err = db.HDel("alldata1", redisKey); err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err := db.RemoveIndex("all$"+userID, redisKey); err != nil {
		fmt.Println("INDEX ALL: ", err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err := db.RemoveIndex("BM"+"$"+userID+"$"+deviceID+"$"+bucket, redisKey); err != nil {
		fmt.Println("INDEX: ", err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err = removeFromListRecordController("BM"+"$"+userID+"$"+deviceID+"$"+bucket, record); err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	// Remove Relationship Record
	go func() {
		relationshipRedisKey := "Relationship$" + userID + "$" + deviceID + "$" + bucket + "$" + record
		if db.Exists(relationshipRedisKey) {
			relationKeys, err := db.LRange(relationshipRedisKey)
			if err != nil {
				fmt.Println("Get Relationship Keys Error: ", err)
			}
			for _, rk := range relationKeys {
				db.LRemove(relationshipRedisKey, rk)
				err = db.Delete(rk)
				if err != nil {
					fmt.Println("Delete Relationship Record Error: ", err)
				}
				if err := db.RemoveIndex("all$"+userID, rk); err != nil {
					fmt.Println("INDEX ALL: ", err)
				}
			}
		}
	}()

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
			if err := utils.KafkaPublishDelete(kafkaProducerDelete, apikey, collection, record); err != nil {
				fmt.Println(err)
			}
		}()
		go publishEvent("Delete", redisKey)
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// SavingData - Lưu data vào DB, thất bại nếu bản ghi đã tồn tại - Error Code 10
func SavingData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	record := c.Param("record")
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

	// Read the Body content
	bodyBytes, err := ioutil.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
	}

	if !middleware.IsStringAlphabetic(record) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	data := make(map[string]interface{})
	bodyResp := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
	}
	data["id"] = record
	data["created_at"] = time.Now().Local().Unix()

	for key, value := range data {
		bodyResp[key] = value
	}

	bodyBytes, err = json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR: ", err)
	}
	// Create Index for Record
	if err = db.SetIndex("BM"+"$"+userID+"$"+deviceID+"$"+bucket, redisKey); err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	if err = db.SetIndex("all$"+userID, redisKey); err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	var args interface{} = "NX"
	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), args)
	if err != nil || ret != "OK" {
		fmt.Println("ERROR: ", err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err = db.HSet("alldata1", redisKey, string(bodyBytes)); err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err = addToListRecordController("BM"+"$"+userID+"$"+deviceID+"$"+bucket, record); err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(bodyBytes)); err != nil {
				fmt.Println(err)
			}
		}()
		go publishEvent("Create", redisKey)
	}
	return c.JSON(201, bodyResp)
}

// UpdateOne - Update 1 field trong 1 Record trong Bucket - Error Code 13
func UpdateOne(c echo.Context) error {

	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	record := c.Param("record")
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
	uri := c.Request().URL.Path
	uri = strings.ReplaceAll(uri, "/api/core/v1/data/update_one/", "")
	uriArr := strings.Split(uri, "/")
	uriArr = middleware.DeleteEmpty(uriArr)
	uriArr = uriArr[2:]
	query := ""
	for _, v := range uriArr {
		if middleware.IsNumber(v) {
			query = query + ".[" + v + "]"
		} else {
			query = query + "." + v
		}
	}
	if query == "" {
		query = "."
	}

	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		defer c.Request().Body.Close()
	} else {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	reqContentType := c.Request().Header.Get("Content-Type")
	bodyStr := string(bodyBytes)
	if !strings.Contains(reqContentType, "application/json") && !strings.Contains(reqContentType, "text/plain") {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if strings.Contains(reqContentType, "text/plain") {
		if bodyStr[0] != '"' && bodyStr[len(bodyStr)-1] != '"' {
			bodyStr = "\"" + bodyStr + "\""
		}
	}

	// Restore the io.ReadCloser to its original state
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	_, err = db.ReJSONUpdate(redisKey, query, bodyStr)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	data, err := db.ReJSONGet(redisKey, ".")
	if err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	bodyBytes, err = json.Marshal(data)
	if err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	if err = db.HSet("alldata1", redisKey, string(bodyBytes)); err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(bodyBytes)); err != nil {
				fmt.Println(err)
			}
		}()
		go publishEvent("Update", redisKey)
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// UpdateMany - Update lại toàn bộ nội dung của 1 Record trong Bucket - Error Code 14
func UpdateMany(c echo.Context) error {

	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	record := c.Param("record")
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
	// Read the Body content
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		defer c.Request().Body.Close()
	}
	// Restore the io.ReadCloser to its original state
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), nil)
	if err != nil || ret != "OK" {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err = db.HSet("alldata1", redisKey, string(bodyBytes)); err != nil {
		fmt.Println(err)
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(bodyBytes)); err != nil {
				fmt.Println(err)
			}
		}()
		go publishEvent("Update", redisKey)
	}

	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// UpdateSomeField - Update một vài field tồn tại trong record. Field cần phải tồn tại trong record. - Error Code 15
func UpdateSomeField(c echo.Context) error {

	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}

	bucket := c.Param("bucket")
	record := c.Param("record")
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
	// Read the Body content
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		defer c.Request().Body.Close()
	}
	// Restore the io.ReadCloser to its original state
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	// Get Origin Data from DB
	originData, err := db.ReJSONGet(redisKey, ".")
	if err != nil || originData == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Update Failed. Record is not Exist"})
	}

	data, err := middleware.ConvertBytesToMap(bodyBytes)
	if err != nil {
		c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Failed"})
	}

	// Convert Interface to Map
	destData := originData.(map[string]interface{})

	mapKeys := reflect.ValueOf(data).MapKeys()
	for _, m := range mapKeys {
		destData[m.String()] = data[m.String()]
	}

	byteDestData, err := json.Marshal(destData)
	if err != nil {
		fmt.Println(err)
		c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Data Failed"})
	}

	// Update New Data to DB
	ret, err := db.ReJSONSet(redisKey, ".", string(byteDestData), nil)
	if err != nil || ret != "OK" {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}

	if err = db.HSet("alldata1", redisKey, string(byteDestData)); err != nil {
		fmt.Println(err)
	}

	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
	if UseKafka {
		go func() {
			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(byteDestData)); err != nil {
				fmt.Println(err)
			}
		}()
		go publishEvent("Update", redisKey)
	}

	return c.JSON(200, destData)
}

//CreateExpireData - Create a Data have Expire time
func CreateExpireData(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	bucket := c.Param("bucket")
	record := c.Param("record")
	expTime, err := strconv.ParseInt(c.Param("time"), 10, 64)
	if err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Expire Time Parameter is Invalid"})
	}
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

	// Read the Body content
	bodyBytes, err := ioutil.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
	}
	data := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println(err)
	}
	if _, err := db.SetExpire(redisKey, data, time.Duration(expTime)*time.Second); err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Expire Data Failed"})
	}
	return c.JSON(200, data)
}

// addToListRecordController - Thêm record vào danh sách record thuộc bucket
func addToListRecordController(redisKey, recordAppend string) error {
	_, err := db.LPush("List$"+redisKey, recordAppend)
	//db.Save()
	return err
}

// removeFromListRecordController - Loại bỏ một record từ danh sách record thuộc bucket
func removeFromListRecordController(redisKey, recordRemove string) error {
	_, err := db.LRemove("List$"+redisKey, recordRemove)
	//db.Save()
	return err
}

func publishEvent(typeEvent string, header string) {
	headerPubEvent := header
	bodyPubEvent := typeEvent + "###" + headerPubEvent
	go utils.KafkaPublishEvent(kafkaProducerEventStore, headerPubEvent, bodyPubEvent)
}
