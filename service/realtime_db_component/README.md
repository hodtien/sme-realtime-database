### Luồng hoạt động của mBaaS Platform - Dynamics Database & Backend as a Service ###
- Login vào Dashboard của mBaaS
- Khởi tạo thiết bị sử dụng dịch vụ
- Khởi tạo Bucket thuộc về thiết bị (một thiết bị có thể có nhiều Bucket). Bucket là nơi lưu trữ các Record.
- Khởi tạo Record thuộc về Bucket (một Bucket có thể có nhiều Record). Record có thể ở dạng JSON Text hoặc ở dạng Object Data (gồm các loại dữ liệu như: hình ảnh, file, video,...)
- Device chỉ có thể được khởi tạo từ Dashboard của mBaaS. Bucket và Record có thể được khởi tạo thông qua Dashboard của mBaaS hoặc thông qua HTTP Rest API. Khi khởi tạo Bucket và Record thông qua HTTP Rest API cần sử dụng API Key được generate khi tạo Device.
- Thông tin của Bucket hoặc dữ liệu của Record được truy xuất thông qua dạng JSON Tree theo chuẩn ECMA-404 The JSON Data Interchange Standard (http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf)