// /*
// * VNPT Business Platform:
// * Xử lý nghiệp vụ chính của Backend as a Service,
// * cung cấp một nền tảng BaaS cho người dùng tương tác và
// * xử lý mà không cần tới việc xây dựng một Backend thực thụ.
// * Người dùng không cần quan tâm với Backend, Database, Hạ tầng triển khai.
//  */

package service

// import (
// 	"bytes"
// 	"encoding/json"
// 	"fmt"
// 	"io/ioutil"
// 	"reflect"
// 	"strconv"
// 	"strings"
// 	"time"

// 	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
// 	utils "bitbucket.org/cloud-platform/sme-realtime-database/service/utils"
// 	"github.com/labstack/echo"
// )

// /*========================== DATA MANAGEMENT ==========================*/

// // RetrieveNumberRecords - Nhận số lượng Records đang có trong bucket
// // RetrieveNumberRecords godoc
// // @Summary Nhận số lượng Records đang có trong bucket
// // @Description Nhận số lượng Records đang có trong bucket
// // @Router /api/core/v1/data/list/:bucket [get]
// func RetrieveNumberRecords(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}
// 	bucket := c.Param("bucket")
// 	redisKey := userID + "$" + deviceID + "$" + bucket
// 	keys := db.Keys(redisKey)
// 	var keysInterface []interface{}
// 	for _, k := range keys {
// 		k = strings.ReplaceAll(k, userID+"$"+deviceID+"$"+bucket+"$", "")
// 		keysInterface = append(keysInterface, k)
// 	}
// 	retData := map[string]interface{}{
// 		"number": len(keys),
// 		"list":   keysInterface,
// 	}
// 	return c.JSON(200, retData)
// }

// // RetrieveTotalRecords - Nhận số lượng Records đang có
// // RetrieveTotalRecords godoc
// // @Summary Nhận số lượng Records đang có
// // @Description Nhận số lượng Records đang có
// // @Router /api/core/v1/data/total [get]
// func RetrieveTotalRecords(c echo.Context) error {
// 	userID := c.Request().Header.Get("userid")
// 	if userID == "" {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
// 	}
// 	redisKey := "all$" + userID
// 	totalMembers, err := db.ZCard(1, redisKey)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "FAILED"})
// 	}

// 	retData := map[string]interface{}{
// 		"number": totalMembers,
// 	}
// 	return c.JSON(200, retData)
// }

// // RetrieveData - Nhận một Record trong Bucket
// // RetrieveData godoc
// // @Summary Nhận một Record trong Bucket
// // @Description Nhận một Record trong Bucket
// // @Router /api/core/v1/data/detail/:bucket/ [get]
// func RetrieveData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	uri := c.Request().URL.Path
// 	uri = strings.ReplaceAll(uri, "/api/core/v1/data/detail/", "")
// 	//key, query := middleware.URIToQuery(uri)
// 	uriArr := strings.Split(uri, "/")
// 	uriArr = middleware.DeleteEmpty(uriArr)
// 	bucket := uriArr[0]
// 	key := uriArr[1]
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + key
// 	uriArr = uriArr[2:]
// 	query := ""
// 	for _, v := range uriArr {
// 		if middleware.IsNumber(v) {
// 			query = query + "[" + v + "]"
// 		} else {
// 			query = query + "." + v
// 		}
// 	}
// 	if query == "" {
// 		query = "."
// 	}
// 	ret, err := db.ReJSONGet(redisKey, query)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
// 	}
// 	respData := map[string]interface{}{
// 		"record_id": key,
// 		"data_item": ret,
// 	}
// 	return c.JSON(200, respData)
// }

// // RetrieveAllDataInBucket - Lấy toàn bộ Records trong 1 Bucket
// // RetrieveAllDataInBucket godoc
// // @Summary Lấy toàn bộ Records trong 1 Bucket
// // @Description Lấy toàn bộ Records trong 1 Bucket
// // @Router /api/core/v1/data/all_in_bucket/:bucket [get]
// func RetrieveAllDataInBucket(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket, query := "", ""
// 	uri := c.Request().URL.Path
// 	uri = strings.Trim(uri, "")
// 	uri = strings.ReplaceAll(uri, "/api/core/v1/data/all_in_bucket/", "")
// 	if uri != "" && strings.Contains(uri, "/") {
// 		uriArr := strings.Split(uri, "/")
// 		bucket = uriArr[0]
// 		uriArr = middleware.DeleteEmpty(uriArr)
// 		uriArr = uriArr[1:]
// 		for _, v := range uriArr {
// 			if middleware.IsNumber(v) {
// 				query = query + "[" + v + "]"
// 			} else {
// 				query = query + "." + v
// 			}
// 		}
// 	} else {
// 		bucket = uri
// 		query = "."
// 	}

// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$"
// 	keys := db.Keys(redisKey)
// 	if len(keys) == 0 {
// 		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	ret, err := db.ReJSONGetAll(redisKey, query, keys, "record", deviceID, bucket)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
// 	}
// 	return c.JSON(200, ret)
// }

// // RetrieveAllData - Lấy toàn bộ Records của User
// // RetrieveAllData godoc
// // @Summary Lấy toàn bộ Records của User
// // @Description Lấy toàn bộ Records của User
// // @Router /api/core/v1/data/all [get]
// func RetrieveAllData(c echo.Context) (err error) {

// 	userID := c.Request().Header.Get("userid")
// 	if userID == "" {
// 		return c.JSON(400, "UserID or DeviceID is Invalid")
// 	}
// 	var start, stop int64
// 	if c.QueryParam("start") == "" || c.QueryParam("stop") == "" {
// 		return c.JSON(400, "Param is invalid")
// 	}
// 	start, err = strconv.ParseInt(c.QueryParam("start"), 10, 64)
// 	if err != nil {
// 		return c.JSON(400, "Param is invalid")
// 	}
// 	stop, err = strconv.ParseInt(c.QueryParam("stop"), 10, 64)
// 	start = start - 1
// 	stop = stop - 1
// 	if err != nil || start > stop {
// 		return c.JSON(400, "Param is invalid")
// 	}

// 	keys, err := db.GetKeysByIndex("all$"+userID, start, stop)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	if len(keys) == 0 {
// 		return c.JSON(200, map[string]interface{}{"code": "0", "message": "null"})
// 	}

// 	redisKey := userID + "$"

// 	//ret, err := db.ReJSONGetAll(redisKey, ".", keys, "record_all")
// 	ret, err := db.HashGetAll(redisKey, ".", keys, "record_all")
// 	if err != nil || ret == nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
// 	}

// 	return c.JSON(200, ret)
// }

// // DeleteData - Xoá một bản ghi đã tồn tại trong DB - Error Code 12
// // DeleteData godoc
// // @Summary Xoá một bản ghi đã tồn tại trong DB
// // @Description Xoá một bản ghi đã tồn tại trong DB
// // @Router /api/core/v1/data/delete/:bucket/ [delete]
// func DeleteData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	uri := c.Request().URL.Path
// 	uri = strings.ReplaceAll(uri, "/api/core/v1/data/delete/", "")
// 	uriArr := strings.Split(uri, "/")
// 	uriArr = middleware.DeleteEmpty(uriArr)
// 	//mgoDocumentID := uriArr[1]
// 	bucket := uriArr[0]
// 	record := uriArr[1]
// 	//key := uriArr[0] + "$" + uriArr[1]
// 	uriArr = uriArr[2:]
// 	query := ""
// 	for _, v := range uriArr {
// 		if middleware.IsNumber(v) {
// 			query = query + "[" + v + "]"
// 		} else {
// 			query = query + "." + v
// 		}
// 	}
// 	if query == "" {
// 		query = "."
// 	}
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

// 	if err = db.Delete(redisKey); err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if _, err = db.HDel("alldata1", redisKey); err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err := db.RemoveIndex("all$"+userID, redisKey); err != nil {
// 		fmt.Println("INDEX ALL: ", err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err := db.RemoveIndex("BM"+"$"+userID+"$"+deviceID+"$"+bucket, redisKey); err != nil {
// 		fmt.Println("INDEX: ", err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err = RemoveFromListRecordController("BM"+"$"+userID+"$"+deviceID+"$"+bucket, record); err != nil {
// 		fmt.Println(err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	// Remove Relationship Record
// 	go func() {
// 		relationshipRedisKey := "Relationship$" + userID + "$" + deviceID + "$" + bucket + "$" + record
// 		if db.Exists(relationshipRedisKey) {
// 			relationKeys, err := db.LRange(relationshipRedisKey)
// 			if err != nil {
// 				fmt.Println("Get Relationship Keys Error: ", err)
// 			}
// 			for _, rk := range relationKeys {
// 				db.LRemove(relationshipRedisKey, rk)
// 				err = db.Delete(rk)
// 				if err != nil {
// 					fmt.Println("Delete Relationship Record Error: ", err)
// 				}
// 				if err := db.RemoveIndex("all$"+userID, rk); err != nil {
// 					fmt.Println("INDEX ALL: ", err)
// 				}
// 			}
// 		}
// 	}()

// 	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
// 	if UseKafka {
// 		go func() {
// 			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
// 			if err := utils.KafkaPublishDelete(kafkaProducerDelete, apikey, collection, record); err != nil {
// 				fmt.Println(err)
// 			}
// 		}()
// 		headerPubEvent := redisKey
// 		bodyPubEvent := "Delte###" + headerPubEvent
// 		go utils.KafkaPublishEvent(kafkaProducerEventStore, headerPubEvent, bodyPubEvent)
// 	}

// 	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
// }

// // SavingData - Lưu data vào DB, thất bại nếu bản ghi đã tồn tại - Error Code 10
// // SavingData godoc
// // @Summary Lưu data vào DB, thất bại nếu bản ghi đã tồn tại
// // @Description Lưu data vào DB, thất bại nếu bản ghi đã tồn tại
// // @Router /api/core/v1/data/create/:bucket/:record [post]
// func SavingData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	record := c.Param("record")
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

// 	// Read the Body content
// 	bodyBytes, err := ioutil.ReadAll(c.Request().Body)
// 	defer c.Request().Body.Close()
// 	if err != nil {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
// 	}

// 	if !middleware.IsStringAlphabetic(record) {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
// 	}

// 	data := make(map[string]interface{})
// 	bodyResp := make(map[string]interface{})
// 	err = json.Unmarshal(bodyBytes, &data)
// 	if err != nil {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
// 	}
// 	data["id"] = record
// 	data["created_at"] = time.Now().Local().Unix()

// 	for key, value := range data {
// 		bodyResp[key] = value
// 	}

// 	bodyBytes, err = json.Marshal(data)
// 	if err != nil {
// 		fmt.Println("ERROR: ", err)
// 	}
// 	// Create Index for Record
// 	if err = db.SetIndex("BM"+"$"+userID+"$"+deviceID+"$"+bucket, redisKey); err != nil {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	if err = db.SetIndex("all$"+userID, redisKey); err != nil {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	var args interface{} = "NX"
// 	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), args)
// 	if err != nil || ret != "OK" {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err = db.HSet("alldata1", redisKey, string(bodyBytes)); err != nil {
// 		fmt.Println(err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err = AddToListRecordController("BM"+"$"+userID+"$"+deviceID+"$"+bucket, record); err != nil {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
// 	if UseKafka {
// 		go func() {
// 			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
// 			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(bodyBytes)); err != nil {
// 				fmt.Println(err)
// 			}
// 		}()
// 		headerPubEvent := redisKey
// 		bodyPubEvent := "Create###" + headerPubEvent
// 		go utils.KafkaPublishEvent(kafkaProducerEventStore, headerPubEvent, bodyPubEvent)
// 	}
// 	return c.JSON(201, bodyResp)
// }

// // RetrieveManyData - Nhận về một vài record theo yêu cầu từ Body Request
// // RetrieveManyData godoc
// // @Summary Nhận về một vài record theo yêu cầu từ Body Request
// // @Description Nhận về một vài record theo yêu cầu từ Body Request
// // @Router /api/core/v1/data/retrieve_many/:bucket [post]
// func RetrieveManyData(c echo.Context) error {

// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")

// 	reqBody := new(RetrieveManyRecordRequest)
// 	if err := c.Bind(reqBody); err != nil {
// 		fmt.Println(err)
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
// 	}
// 	var listRecord []string
// 	redisKey := userID + "$" + deviceID + "$" + bucket
// 	if strings.Contains(reqBody.List, ",") {
// 		listRecord = strings.Split(reqBody.List, ",")
// 		for i, record := range listRecord {
// 			listRecord[i] = redisKey + "$" + strings.Trim(record, " ")
// 		}
// 	} else {
// 		listRecord = append(listRecord, redisKey+"$"+reqBody.List)
// 	}
// 	ret, err := db.ReJSONGetAll(redisKey, ".", listRecord, "record_all")
// 	if err != nil || ret == nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
// 	}

// 	return c.JSON(200, ret)
// }

// // UpdateOne - Update 1 field trong 1 Record trong Bucket - Error Code 13
// // UpdateOne godoc
// // @Summary Update 1 field trong 1 Record trong Bucket
// // @Description Update 1 field trong 1 Record trong Bucket
// // @Router /api/core/v1/data/update_one/:bucket/:record/ [patch]
// func UpdateOne(c echo.Context) error {

// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	record := c.Param("record")
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
// 	uri := c.Request().URL.Path
// 	uri = strings.ReplaceAll(uri, "/api/core/v1/data/update_one/", "")
// 	uriArr := strings.Split(uri, "/")
// 	uriArr = middleware.DeleteEmpty(uriArr)
// 	uriArr = uriArr[2:]
// 	query := ""
// 	for _, v := range uriArr {
// 		if middleware.IsNumber(v) {
// 			query = query + ".[" + v + "]"
// 		} else {
// 			query = query + "." + v
// 		}
// 	}
// 	if query == "" {
// 		query = "."
// 	}

// 	var bodyBytes []byte
// 	if c.Request().Body != nil {
// 		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
// 		defer c.Request().Body.Close()
// 	} else {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
// 	}
// 	reqContentType := c.Request().Header.Get("Content-Type")
// 	bodyStr := string(bodyBytes)
// 	if !strings.Contains(reqContentType, "application/json") && !strings.Contains(reqContentType, "text/plain") {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
// 	}
// 	if strings.Contains(reqContentType, "text/plain") {
// 		if bodyStr[0] != '"' && bodyStr[len(bodyStr)-1] != '"' {
// 			bodyStr = "\"" + bodyStr + "\""
// 		}
// 	}

// 	// Restore the io.ReadCloser to its original state
// 	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

// 	_, err = db.ReJSONUpdate(redisKey, query, bodyStr)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	data, err := db.ReJSONGet(redisKey, ".")
// 	if err != nil {
// 		fmt.Println(err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	bodyBytes, err = json.Marshal(data)
// 	if err != nil {
// 		fmt.Println(err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	if err = db.HSet("alldata1", redisKey, string(bodyBytes)); err != nil {
// 		fmt.Println(err)
// 		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
// 	if UseKafka {
// 		go func() {
// 			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
// 			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(bodyBytes)); err != nil {
// 				fmt.Println(err)
// 			}
// 		}()
// 		headerPubEvent := redisKey
// 		bodyPubEvent := "Update###" + headerPubEvent
// 		go utils.KafkaPublishEvent(kafkaProducerEventStore, headerPubEvent, bodyPubEvent)
// 	}

// 	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
// }

// // UpdateMany - Update lại toàn bộ nội dung của 1 Record trong Bucket - Error Code 14
// // UpdateMany godoc
// // @Summary Update lại toàn bộ nội dung của 1 Record trong Bucket
// // @Description Update lại toàn bộ nội dung của 1 Record trong Bucket
// // @Router /api/core/v1/update_many/:bucket/:record [patch]
// func UpdateMany(c echo.Context) error {

// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	record := c.Param("record")
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
// 	// Read the Body content
// 	var bodyBytes []byte
// 	if c.Request().Body != nil {
// 		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
// 		defer c.Request().Body.Close()
// 	}
// 	// Restore the io.ReadCloser to its original state
// 	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

// 	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), nil)
// 	if err != nil || ret != "OK" {
// 		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err = db.HSet("alldata1", redisKey, string(bodyBytes)); err != nil {
// 		fmt.Println(err)
// 	}

// 	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
// 	if UseKafka {
// 		go func() {
// 			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
// 			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(bodyBytes)); err != nil {
// 				fmt.Println(err)
// 			}
// 		}()
// 		headerPubEvent := redisKey
// 		bodyPubEvent := "Update###" + headerPubEvent
// 		go utils.KafkaPublishEvent(kafkaProducerEventStore, headerPubEvent, bodyPubEvent)
// 	}

// 	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
// }

// // UpdateSomeField - Update một vài field tồn tại trong record. Field cần phải tồn tại trong record. - Error Code 15
// // UpdateSomeField godoc
// // @Summary Update một vài field tồn tại trong record. Field cần phải tồn tại trong record.
// // @Description Update một vài field tồn tại trong record. Field cần phải tồn tại trong record.
// // @Router /api/core/v1/data/update_field/:bucket/:record [patch]
// func UpdateSomeField(c echo.Context) error {

// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	record := c.Param("record")
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record
// 	// Read the Body content
// 	var bodyBytes []byte
// 	if c.Request().Body != nil {
// 		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
// 		defer c.Request().Body.Close()
// 	}
// 	// Restore the io.ReadCloser to its original state
// 	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

// 	// Get Origin Data from DB
// 	originData, err := db.ReJSONGet(redisKey, ".")
// 	if err != nil || originData == "" {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Update Failed. Record is not Exist"})
// 	}

// 	data, err := middleware.ConvertBytesToMap(bodyBytes)
// 	if err != nil {
// 		c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Failed"})
// 	}

// 	// Convert Interface to Map
// 	destData := originData.(map[string]interface{})

// 	mapKeys := reflect.ValueOf(data).MapKeys()
// 	for _, m := range mapKeys {
// 		destData[m.String()] = data[m.String()]
// 	}

// 	byteDestData, err := json.Marshal(destData)
// 	if err != nil {
// 		fmt.Println(err)
// 		c.JSON(500, map[string]interface{}{"code": "-1", "message": "Update Data Failed"})
// 	}

// 	// Update New Data to DB
// 	ret, err := db.ReJSONSet(redisKey, ".", string(byteDestData), nil)
// 	if err != nil || ret != "OK" {
// 		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}

// 	if err = db.HSet("alldata1", redisKey, string(byteDestData)); err != nil {
// 		fmt.Println(err)
// 	}

// 	// v1.2.0: Sync to Persistent Database (MongoDB) over Kafka
// 	if UseKafka {
// 		go func() {
// 			collection := "BM" + "@" + userID + "@" + deviceID + "@" + bucket
// 			if err = utils.KafkaPublishSaveOrUpdate(kafkaProducerDataSOU, apikey, collection, record, string(byteDestData)); err != nil {
// 				fmt.Println(err)
// 			}
// 		}()
// 		headerPubEvent := redisKey
// 		bodyPubEvent := "Update###" + headerPubEvent
// 		go utils.KafkaPublishEvent(kafkaProducerEventStore, headerPubEvent, bodyPubEvent)
// 	}

// 	return c.JSON(200, destData)
// }

// //SearchData - Tìm kiếm một Data được lưu trong Persistent MongoDB
// func SearchData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}
// 	bucket := c.Param("bucket")
// 	field := c.QueryParam("field")
// 	pattern := c.QueryParam("pattern")
// 	retSearch, b := mgodb.SearchInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, field, pattern)
// 	if !b {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
// 	}
// 	return c.JSON(200, retSearch)
// }

// //MatchData - Query All Data được lưu trong Persistent MongoDB theo điều kiện được match
// func MatchData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}
// 	var bodyBytes []byte
// 	if c.Request().Body != nil {
// 		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
// 		defer c.Request().Body.Close()
// 	}
// 	// Restore the io.ReadCloser to its original state
// 	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
// 	data := make(map[string]interface{})
// 	err = json.Unmarshal(bodyBytes, &data)
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	fields := make([]string, 0, len(data))
// 	values := make([]string, 0, len(data))
// 	for f, v := range data {
// 		fields = append(fields, f)
// 		tmp := fmt.Sprintf("%s", v)
// 		values = append(values, tmp)
// 	}

// 	bucket := c.Param("bucket")
// 	retSearch, b := mgodb.SearchManyConditionInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, fields, values)
// 	if !b {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
// 	}
// 	return c.JSON(200, retSearch)
// }

// //SortData - Sort Data theo Field
// func SortData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	fieldSort := c.QueryParam("field")
// 	retSort, err := mgodb.SortInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, fieldSort)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
// 	}
// 	return c.JSON(200, retSort)
// }

// //LimitData - Giới hạn dữ liệu nhận về, có thể sort hoặc không.
// func LimitData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	limit, err := strconv.Atoi(c.QueryParam("limit"))
// 	if err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Param is invalid"})
// 	}
// 	fieldSort := c.QueryParam("field")

// 	retSort, err := mgodb.LimitSortInMongo("BM"+"@"+userID+"@"+deviceID+"@"+bucket, fieldSort, limit)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
// 	}
// 	return c.JSON(200, retSort)
// }

// // PaginateWithSkip - API lấy dữ liệu theo phân trang
// func PaginateWithSkip(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}
// 	bucket := c.Param("bucket")
// 	limit, err := strconv.Atoi(c.QueryParam("limit"))
// 	if err != nil {
// 		return c.JSON(400, "Param is invalid")
// 	}
// 	page, err := strconv.Atoi(c.QueryParam("page"))
// 	if err != nil {
// 		return c.JSON(400, "Param is invalid")
// 	}

// 	retPagging, err := mgodb.PaginateWithSkip("BM"+"@"+userID+"@"+deviceID+"@"+bucket, page, limit)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not Found"})
// 	}
// 	return c.JSON(200, retPagging)
// }

// //CreateExpireData - Create a Data have Expire time
// func CreateExpireData(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}
// 	bucket := c.Param("bucket")
// 	record := c.Param("record")
// 	expTime, err := strconv.ParseInt(c.Param("time"), 10, 64)
// 	if err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Expire Time Parameter is Invalid"})
// 	}
// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

// 	// Read the Body content
// 	bodyBytes, err := ioutil.ReadAll(c.Request().Body)
// 	defer c.Request().Body.Close()
// 	if err != nil {
// 		fmt.Println("ERROR: ", err)
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
// 	}
// 	data := make(map[string]interface{})
// 	err = json.Unmarshal(bodyBytes, &data)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	if _, err := db.SetExpire(redisKey, data, time.Duration(expTime)*time.Second); err != nil {
// 		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Create Expire Data Failed"})
// 	}
// 	return c.JSON(200, data)
// }

// // GetFieldDetailInAllRecord - GetFieldDetailInAllRecord
// func GetFieldDetailInAllRecord(c echo.Context) error {
// 	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
// 	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
// 	if deviceID == "" || userID == "" || err != nil {
// 		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
// 	}

// 	bucket := c.Param("bucket")
// 	query := c.QueryParam("fieldName")

// 	redisKey := userID + "$" + deviceID + "$" + bucket + "$"
// 	keys := db.Keys(redisKey)
// 	if len(keys) == 0 {
// 		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
// 	}
// 	ret, err := db.ReJSONGetAll(redisKey, query, keys, "record", deviceID, bucket)
// 	if err != nil {
// 		return c.JSON(200, map[string]interface{}{"code": "1", "data": "Not found"})
// 	}
// 	return c.JSON(200, map[string]interface{}{"code": "0", "data": ret})
// }
