/*
* VNPT Business Platform:
* Xử lý nghiệp vụ chính của Storage as a Service,
* cung cấp cho người dùng một nền tảng cho phép upload/download
* và quản trị các tài nguyên liên quan tới dữ liệu Multimedia.
* Người dùng không cần quan tâm với Backend, Database, Hạ tầng triển khai.
 */

package service

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"github.com/labstack/echo"
)

/*========================== MEDIA MANAGEMENT ==========================*/

// SwiftUploadFile - Swift Upload Media File
// SwiftUploadFile godoc
// @Summary Upload File tới một Bucket trên Swift Storage
// @Description Upload File tới một Bucket trên Swift Storage
// @Router /api/core/v1/swift/upload/:bucket [post]
func SwiftUploadFile(c echo.Context) error {
	//--------------------------
	// Read files Multipart form
	//--------------------------

	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(http.StatusBadRequest, "UserID or DeviceID is Invalid")
	}

	bucket := c.Param("bucket")
	//record := c.Param("record")

	// Lấy form data
	form, err := c.MultipartForm()
	if err != nil {
		log.Println("Failed to get form data, detail error:", err)
		return c.JSON(http.StatusOK, map[string]interface{}{"status": "0", "message": "Upload failed"})
	}

	// Lấy danh sách file trong form với key là "data"
	files := form.File["data"]
	if files == nil {
		log.Println("No files need to upload")
		return c.JSON(http.StatusOK, map[string]interface{}{"status": "0", "message": "No data available"})
	}

	// Log data client request to console log
	log.Println("Client request api upload: ", files)

	// Upload file lên swift và update url qua bên Core
	fileName, contentType := "", ""
	container := bucket
	var infoMediaUploadArr []interface{}

	for _, file := range files {
		// Lấy nội dung file
		src, err := file.Open()
		if err != nil {
			log.Println("Failed to open file, detail error:", err)
			continue
		}
		defer src.Close()

		currentTime := time.Now().Local()
		timeUpload := currentTime.Format("02-01-2006 15-00-00")

		buf, err := ioutil.ReadAll(src)
		if err != nil {
			log.Println("Failed to read file source, detail error:", err)
			continue
		}
		// Lấy định dạng file
		contentType = http.DetectContentType(buf)
		fileType := ""
		if strings.Contains(contentType, "octet-stream") {
			fileType = "mp4"
		} else {
			fileType = strings.Split(contentType, "/")[1]
		}
		if strings.HasSuffix(file.Filename, ".png") || strings.HasSuffix(file.Filename, ".png") {
			fileName = file.Filename
		} else {
			fileName = file.Filename + "." + fileType
		}

		// upload file to swift
		go db.UploadToSwift(container, fileName, buf, contentType)

		infoMediaUpload := map[string]interface{}{
			"id":           fileName,
			"content_type": contentType,
			"path":         "http://203.162.141.77/" + container + "/" + fileName,
			"upload_time":  timeUpload,
		}
		bodyBytes, err := json.Marshal(infoMediaUpload)
		if err != nil {
			return c.JSON(http.StatusOK, infoMediaUpload)
		}

		record := fileName
		redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

		// Lưu thông tin File đã được Upload vào RedisDB
		ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), nil)
		if err != nil || ret != "OK" {
			return c.JSON(http.StatusOK, infoMediaUpload)
		}
		infoMediaUploadArr = append(infoMediaUploadArr, infoMediaUpload)
	}

	// go func() {
	// 	db.SaveMongo("media", infoMediaUpload)
	// }()

	return c.JSON(http.StatusOK, infoMediaUploadArr)
}

// SwiftDeleteFile - Swift Xoá một Media Record đã tồn tại trong DB và các File tương ứng được lưu trữ trong Swift Storage
// SwiftUploadFile godoc
// @Summary Xoá một Media Record đã tồn tại trong DB và các File tương ứng được lưu trữ trong Swift Storage
// @Description Xoá một Media Record đã tồn tại trong DB và các File tương ứng được lưu trữ trong Swift Storage
// @Router /api/core/v1/swift/upload/:bucket [delete]
func SwiftDeleteFile(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(http.StatusBadRequest, "UserID or DeviceID is Invalid")
	}
	uri := c.Request().URL.Path
	uri = strings.ReplaceAll(uri, "/api/core/v1/media/delete/", "")
	uriArr := strings.Split(uri, "/")
	uriArr = middleware.DeleteEmpty(uriArr)
	//mgoDocumentID := uriArr[1]
	bucket := c.Param("bucket")
	record := c.Param("record")
	redisKey := userID + "$" + deviceID + "$" + bucket + "$" + record

	ret, err := db.ReJSONGet(redisKey, ".path")
	if err != nil {
		return c.JSON(200, "Delete Multimedia File Failed")
	}

	var mediaData string = ret.(string)
	media := strings.ReplaceAll(mediaData, "http://203.162.141.77/", "")
	container := strings.Split(media, "/")[0]
	fileName := strings.Split(media, "/")[1]
	if err := db.DeleteFile(container, fileName); err != nil {
		return c.JSON(200, "Delete Multimedia File Failed")
	}

	err = db.Delete(redisKey)
	if err != nil {
		return c.JSON(200, "Delete Multimedia File Failed")
	}
	// go func() {
	// 	fmt.Println(mgoDocumentID)
	// 	db.DeleteInMongo("media", mgoDocumentID)
	// }()
	return c.JSON(200, "OK")
}
