package service

import (
	"fmt"
	"strings"
)

// AddToListBucketController - Cập nhật danh sách bucket thuộc device
func AddToListBucketController(redisKey, bucketAppend string) error {
	_, err := db.LPush("List$"+redisKey, bucketAppend)
	//db.Save()
	return err
}

// RemoveFromListBucketController - Loại bỏ một bucket từ danh sách bucket thuộc device
func RemoveFromListBucketController(redisKey, recordRemove string) error {
	_, err := db.LRemove("List$"+redisKey, recordRemove)
	//db.Save()
	return err
}

//GetListBucketController - Nhận về danh sách Bucket thuộc Device
func GetListBucketController(redisKey string) ([]string, error) {
	ret, err := db.LRange("List$" + redisKey)
	return ret, err
}

//DeleteListBucketController - Xoá một hoặc nhiều List Bucket của Device
func DeleteListBucketController(redisKey ...string) {
	db.LDel(redisKey...)
}

// DeleteBucketInDeviceController - Delete all bucket in device
func DeleteBucketInDeviceController(redisKey string) {
	DeleteAllBucket(redisKey)
}

// DeleteDeviceController - Xoá một Device và các dữ liệu thuộc về nó
func DeleteDeviceController(redisKey string) {
	db.Delete("DM" + "$" + redisKey)
	keysDM := "List$" + "DM" + "$" + redisKey
	DeleteListBucketController(keysDM)
	DeleteAllBucket(redisKey)
}

// DeleteAllDeviceController - Delete All Device And Bucket in Device
func DeleteAllDeviceController(redisKeyPattern string) {
	db.DeleteAll("DM" + "$" + redisKeyPattern)
	keysDM := db.LKeys("List$" + "DM" + "$" + redisKeyPattern)
	DeleteListBucketController(keysDM...)
	DeleteAllBucket(redisKeyPattern)
}

//DeleteListRecordController - Xoá một hoặc nhiều List Record của Bucket
func DeleteListRecordController(redisKey ...string) {
	db.LDel(redisKey...)
}

// DeleteDataInBucketController - Xoá tất cả Record trong bucket
func DeleteDataInBucketController(redisKey string) {
	// Lấy danh sách Index ID của Record
	members := db.Keys(redisKey)

	// Xoá toàn bộ Record thuộc bucket
	db.DeleteAll(redisKey)

	// Xoá toàn bộ Index của các Record đã xoá
	key := "all$" + strings.Split(redisKey, "$")[0]
	for _, member := range members {
		_, err := db.ZRem(1, key, member)
		fmt.Println(err)
	}
}

// DeleteBucketController - Xoá một bucket và các dữ liệu thuộc về nó
func DeleteBucketController(redisKey string) {
	db.Delete("BM" + "$" + redisKey)
	keyBM := "List$" + "BM" + "$" + redisKey
	DeleteListRecordController(keyBM)
	DeleteDataInBucketController(redisKey)
}

// DeleteAllBucket - Xoá tất cả Bucket và Record của Bucket
func DeleteAllBucket(redisKeyPattern string) {
	db.DeleteAll("BM" + "$" + redisKeyPattern)
	keysBM := db.LKeys("List$" + "BM" + "$" + redisKeyPattern)
	DeleteListRecordController(keysBM...)
	DeleteDataInBucketController(redisKeyPattern)
}

// AddToListRecordController - Thêm record vào danh sách record thuộc bucket
func AddToListRecordController(redisKey, recordAppend string) error {
	_, err := db.LPush("List$"+redisKey, recordAppend)
	//db.Save()
	return err
}

// RemoveFromListRecordController - Loại bỏ một record từ danh sách record thuộc bucket
func RemoveFromListRecordController(redisKey, recordRemove string) error {
	_, err := db.LRemove("List$"+redisKey, recordRemove)
	//db.Save()
	return err
}

//GetListRecordController - Nhận về danh sách Record thuộc Bucket
func GetListRecordController(redisKey string) ([]string, error) {
	ret, err := db.LRange("List$" + redisKey)
	return ret, err
}
