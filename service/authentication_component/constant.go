package service

import (
	"bitbucket.org/cloud-platform/sme-realtime-database/repository"
)

var db repository.Cache

//Init - Init
// func Init() {
// 	db.CreateCache()
// }

//ETokenDevice - ID's Device receive eToken
type ETokenDevice struct {
	DeviceID string `json:"device_id"`
	LinkCode string `json:"link_code"`
}

//ETokenData - ETokenData
type ETokenData struct {
	EToken   string `json:"e_token"`
	LinkCode string `json:"link_code"`
}
