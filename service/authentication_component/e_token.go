package service

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/model"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

// RegistryServiceUseEToken - RegistryServiceUseEToken
func RegistryServiceUseEToken(c echo.Context) error {
	eTokenDevice := new(ETokenDevice)
	err := c.Bind(eTokenDevice)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if eTokenDevice.DeviceID == "" || eTokenDevice.LinkCode == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	eTokenDeviceID := eTokenDevice.DeviceID
	eTokenLinkCode := "etoken" + "$" + eTokenDevice.LinkCode + "$" + eTokenDeviceID
	if db.Exists(eTokenLinkCode) {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Device was Registried"})
	}
	_, err = db.LPush("listetoken"+"$"+eTokenDeviceID, eTokenDevice.LinkCode)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device Register is Failed"})
	}
	_, err = db.Set(eTokenLinkCode, eTokenDeviceID)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device Register is Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// UnRegistryerviceUseEToken - UnRegistryerviceUseEToken
func UnRegistryerviceUseEToken(c echo.Context) error {
	eTokenDevice := new(ETokenDevice)
	err := c.Bind(eTokenDevice)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if eTokenDevice.DeviceID == "" || eTokenDevice.LinkCode == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	eTokenLinkCode := "etoken" + "$" + eTokenDevice.LinkCode + "$" + eTokenDevice.DeviceID
	if db.Exists(eTokenLinkCode) {
		db.DeleteAll(eTokenLinkCode)
		err = db.Delete("SSO$etoken$" + eTokenDevice.LinkCode)
		if err != nil {
			return c.JSON(200, map[string]interface{}{"code": "-1", "message": "UnRegistry Failed"})
		}
		_, err = db.LPop("listetoken" + "$" + eTokenDevice.DeviceID)
		if err != nil {
			return c.JSON(200, map[string]interface{}{"code": "-1", "message": "UnRegistry Failed"})
		}
		return c.JSON(200, map[string]interface{}{"code": "0", "message": "UnRegistry Success"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "Device is UnRegistried"})
}

// ValidateServiceRegistriedEToken - ValidateServiceRegistriedEToken
func ValidateServiceRegistriedEToken(c echo.Context) error {
	eTokenDevice := new(ETokenDevice)
	err := c.Bind(eTokenDevice)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if eTokenDevice.DeviceID == "" || eTokenDevice.LinkCode == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	eTokenDeviceID := "etoken" + "$" + eTokenDevice.DeviceID
	if !db.Exists(eTokenDeviceID) {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device is not Registered"})
	}
	listServiceRegistried, err := db.LRange("list" + eTokenDeviceID)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device is not Registered"})
	}
	for _, v := range listServiceRegistried {
		if v == eTokenDevice.LinkCode {
			return c.JSON(200, map[string]interface{}{"code": "0", "message": "Device is Registried"})
		}
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "Device is not Registered"})
}

//GenerateETokenData - Generate a eToken Data with 60s Expire Time
func GenerateETokenData(c echo.Context) error {
	eTokenDevice := new(ETokenDevice)
	err := c.Bind(eTokenDevice)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if eTokenDevice.DeviceID == "" || eTokenDevice.LinkCode == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	eTokenLinkCode := "etoken" + "$" + eTokenDevice.LinkCode + "$" + eTokenDevice.DeviceID
	if !db.Exists(eTokenLinkCode) {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device is not Registried"})
	}
	deviceID, err := db.Get(eTokenLinkCode)
	if err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device is not Registried"})
	}
	if deviceID != eTokenDevice.DeviceID {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Device is not Registried"})
	}
	for {
		eTokenData := middleware.GenerateRandomWithDigit(6)
		if !db.Exists(eTokenData) {
			db.SetNX("etoken"+"$"+eTokenDevice.LinkCode+"$"+eTokenData, eTokenDevice.LinkCode, 60*time.Second)
			return c.JSON(200, map[string]interface{}{"code": "0", "e-Token": eTokenData})
		}
	}
}

//ValidateETokenData - Validate a eToken Data
func ValidateETokenData(c echo.Context) error {
	eTokenData := new(ETokenData)
	err := c.Bind(eTokenData)
	if err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if eTokenData.LinkCode == "" || eTokenData.EToken == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}
	if !db.Exists("etoken" + "$" + eTokenData.LinkCode + "$" + eTokenData.EToken) {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "e-Token is Invalid or Expired"})
	}
	eTokenDataLinkCode, err := db.Get("etoken" + "$" + eTokenData.LinkCode + "$" + eTokenData.EToken)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "e-Token is Invalid or Expired"})
	}
	if eTokenDataLinkCode != eTokenData.LinkCode {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "e-Token is Invalid or Expired"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "e-Token is Valid"})
}

// GenerateLinkCodeEToken - Tạo mã liên kết cho 3rd Application sử dụng eToken
func GenerateLinkCodeEToken(c echo.Context) error {
	eTokenObj := new(model.EToken)
	err := c.Bind(eTokenObj)
	if err != nil {
		return c.JSON(400, map[string]interface{}{"code": "0", "message": "Body is Invalid"})
	}
	id, err := uuid.NewUUID()
	if err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "0", "message": "Generate Link Code Failed"})
	}

	bodyData := map[string]interface{}{
		"app_id":    eTokenObj.AppID,
		"app_name":  eTokenObj.AppName,
		"link_code": id.String(),
	}

	bodyBytes, err := json.Marshal(bodyData)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Generate Link Code Failed"})
	}

	var args interface{} = "NX"
	_, err = db.ReJSONSet("SSO$etoken$"+eTokenObj.AppID, ".", string(bodyBytes), args)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Link Code is Exists"})
	}
	fmt.Printf(id.String())
	return c.JSON(200, map[string]interface{}{"code": "0", "message": map[string]interface{}{"link_code": id.String()}})
}

// DeleteLinkCodeEToken - DeleteLinkCodeEToken
func DeleteLinkCodeEToken(c echo.Context) error {
	appID := c.Param("app_id")
	if appID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "App ID is Invalid"})
	}
	err := db.Delete("SSO$etoken$" + appID)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Delete Link Code Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// GetLinkCodeEToken - GetLinkCodeEToken
func GetLinkCodeEToken(c echo.Context) error {
	appID := c.Param("app_id")
	if appID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "App ID is Invalid"})
	}
	data, err := db.ReJSONGet("SSO$etoken$"+appID, ".")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "0", "message": "No Data Available"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": data})
}

// GetAllLinkCodeEToken - GetAllLinkCodeEToken
func GetAllLinkCodeEToken(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User ID is Invalid"})
	}
	redisKey := "SSO$etoken$"
	keys := db.Keys(redisKey)
	if len(keys) == 0 {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	data, err := db.ReJSONGetAll(redisKey, ".", keys, "")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "0", "message": "No Data Available"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": data})
}
