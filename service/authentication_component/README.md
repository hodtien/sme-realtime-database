### Luồng hoạt động của VNPT e-Token ###
- Tải về ứng dụng VNPT e-Token trên Mobile.
- Ứng dụng X muốn sử dụng xác thực 2 lớp của VNPT e-Token. Ứng dụng này cần thực hiện tích hợp với API Validate e-Token và API khởi tạo mã liên kết với VNPT e-Token của VNPT Business Platform.
- Người dùng sử dụng ứng dụng X sau khi đăng nhập có thể vào mục thiết lập người dùng trên ứng dụng X, thực hiện Enable tính năng 2FA với VNPT e-Token => khi này ứng dụng X khởi tạo mã liên kết và trả về mã liên kết này về cho người dùng ứng dụng X.
- Lúc này, người dùng mở ứng dụng VNPT e-Token thực hiện đăng ký ứng dụng X với mã liên kết được trả về ở trên tại app VNPT e-Token.
- Sau khi quá trình đăng ký hoàn tất, ứng dụng VNPT e-Token sẽ thực hiện Generate liên tục một mã xác thực TOTP để thực hiện xác thực 2FA trên ứng dụng X.
- Từ những lần sử dụng tiếp theo, khi người dùng thực hiện đăng nhập trên ứng dụng X, người dùng cần thực hiện nhập thêm mã e-Token (khi mở app VNPT e-Token sẽ thấy mã) để có thể đăng nhập thành công.
- Có thể tắt quá trình 2FA với VNPT e-Token thông qua ứng dụng X và từ ứng dụng VNPT e-Token (Huỷ liên kết ứng dụng X) 