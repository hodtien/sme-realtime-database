package service

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"bitbucket.org/cloud-platform/sme-realtime-database/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

// UserRegister - Phục vụ cho Đăng ký tài khoản mới được quản lý trực tiếp bởi mBaaS
func UserRegister(c echo.Context) error {
	bucketID := c.Param("bucket_id")
	user := new(model.UserMgnt)
	err := c.Bind(user)
	if err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	if !middleware.IsStringAlphabetic(user.Username) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	if userIsExists := db.Exists("UserMgnt$" + user.Username); userIsExists {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Registration Failed. User is Exists"})
	}

	userID, err := uuid.NewUUID()
	if err != nil {
		fmt.Println(err)
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Register UserID Failed"})
	}
	user.UserID = userID.String()
	if user.Role == "" {
		user.Role = "end_user"
	}
	bodyBytes, err := json.Marshal(&user)
	if err != nil {
		fmt.Println(err)
	}

	redisKey := "UserMgnt$" + bucketID + "$" + user.Username

	var args interface{} = "NX"

	ret, err := db.ReJSONSet(redisKey, ".", string(bodyBytes), args)
	if err != nil || ret != "OK" {
		fmt.Println("ERROR: ", err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Failed"})
	}
	return c.JSON(201, map[string]interface{}{"code": "0", "message": "Success"})
}

// UserLogin - Phục vụ cho Login với User được quản lý trực tiếp bởi mBaaS
func UserLogin(c echo.Context) error {
	bucketID := c.Param("bucket_id")
	dataLogin := new(model.DataLogin)
	err := c.Bind(dataLogin)
	if err != nil || dataLogin.Username == "" || dataLogin.Password == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Username or Password Invalid"})
	}

	if !middleware.IsStringAlphabetic(dataLogin.Username) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	redisKey := "UserMgnt$" + bucketID + "$" + dataLogin.Username
	if !db.Exists(redisKey) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User is not Exist"})
	}

	username, err := db.ReJSONGet(redisKey, ".username")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Login Failed"})
	}

	password, err := db.ReJSONGet(redisKey, ".password")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Login Failed"})
	}

	roles, err := db.ReJSONGet(redisKey, ".roles")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Login Failed"})
	}

	userInfoAfterLogin, err := db.ReJSONGet(redisKey, ".")
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Login Failed"})
	}

	// Check in your db if the user exists or not
	if username == dataLogin.Username && password == dataLogin.Password {
		// Create token
		token := jwt.New(jwt.SigningMethodHS256)
		// Set claims
		// This is the information which frontend can use
		// The backend can also decode the token and get admin etc.
		claims := token.Claims.(jwt.MapClaims)
		claims["information"] = userInfoAfterLogin
		claims["roles"] = roles
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Generate encoded token and send it as response.
		// The signing string should be secret (a generated UUID works too)
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, map[string]string{
			"token": t,
		})
	}
	return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Login Failed"})
}

// DeleteUser - DeleteUser
func DeleteUser(c echo.Context) error {
	bucketID := c.Param("bucket_id")
	dataLogin := new(model.DataLogin)
	err := c.Bind(dataLogin)
	if err != nil || dataLogin.Username == "" || dataLogin.Password == "" {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Username or Password Invalid"})
	}

	if !middleware.IsStringAlphabetic(dataLogin.Username) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body is Invalid"})
	}

	redisKey := "UserMgnt$" + bucketID + "$" + dataLogin.Username
	if !db.Exists(redisKey) {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "User is not Exist"})
	}

	if username, err := db.ReJSONGet(redisKey, ".username"); err != nil || username == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Delete User Failed"})
	}

	if password, err := db.ReJSONGet(redisKey, ".password"); err != nil || password == "" {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Delete User Failed"})
	}

	if err := db.Delete(redisKey); err != nil {
		return c.JSON(200, map[string]interface{}{"code": "-1", "message": "Delete User Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "Delete User Success"})
}
