package service

// package main

// import (
// 	"fmt"
// 	"net/http"
// 	"time"

// 	"golang.org/x/sync/singleflight"
// )

// var requestGroup singleflight.Group

// func prepare() {
// 	fmt.Println("Init cache")
// 	CreateCache()
// 	CacheRedis.DeleteAllKeys()
// 	Client = &http.Client{
// 		Timeout: time.Second * 30,
// 	}
// 	FlagDeleteKey = false
// 	go CacheRedis.CheckUpdateAll()
// 	go CheckTokenExpireInKey()
// }

// func main() {
// 	prepare()

// 	fmt.Println("Ready to serve")

// 	server := &http.Server{
// 		Addr:         ":" + getEnv("PORT", "8080"),
// 		WriteTimeout: 30 * time.Second,
// 		ReadTimeout:  30 * time.Second,
// 		Handler:      http.HandlerFunc(handleRequest),
// 	}

// 	err := server.ListenAndServe()
// 	if err != nil {
// 		fmt.Println(err.Error())
// 	}
// }

// func handleRequest(w http.ResponseWriter, r *http.Request) {

// 	fullURL := r.URL.Path + "?" + r.URL.RawQuery
// 	fmt.Printf("Requested '%s' '%s'\n", r.Method, fullURL)

// 	if r.Method != "GET" {
// 		ForwardRequest(w, r, fullURL)
// 	} else {
// 		if CacheRedis.Has(fullURL) {
// 			for {
// 				if !FlagDeleteKey {
// 					break
// 				}
// 			}
// 			UseCache(w, r, fullURL)
// 		} else {
// 			keyDedupe := r.URL.Path
// 			dedupingHandle(keyDedupe, fullURL, w, r)
// 		}
// 	}
// }

// func dedupingHandle(dedupeKey, fullURL string, w http.ResponseWriter, r *http.Request) {
// 	_, err, shared := requestGroup.Do(dedupeKey, func() (interface{}, error) {
// 		CreateRedisKey(w, r, fullURL)
// 		return "", nil
// 	})

// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}

// 	fmt.Printf("CreateRedisKey handler request: shared result %t", shared)
// 	fmt.Fprintf(w, "")
// }
