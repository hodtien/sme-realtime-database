package service

import (
	//"fmt"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

// Cache - Redis Cache Struct
type Cache struct {
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

var redisClient *redis.Client
var redisClient2 *redis.Client
var redisClient3 *redis.Client

// CreateCache - Create Redis Connection
func CreateCache() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     getEnv("REDIS_HOST", "203.162.141.98") + ":" + getEnv("REDIS_PORT", "6379"),
		Password: "ReDis0rimx",
		DB:       0,
	})
	redisClient2 = redis.NewClient(&redis.Options{
		Addr:     getEnv("REDIS_HOST", "203.162.141.98") + ":" + getEnv("REDIS_PORT", "6379"),
		Password: "ReDis0rimx",
		DB:       1,
	})
	redisClient3 = redis.NewClient(&redis.Options{
		Addr:     getEnv("REDIS_HOST", "203.162.141.98") + ":" + getEnv("REDIS_PORT", "6379"),
		Password: "ReDis0rimx",
		DB:       2,
	})
}

// Has - Check key is exist
func (c *Cache) Has(key string) bool {
	total, err := redisClient.Exists(key).Result()
	if err != nil {
		panic(err)
	}
	if total > 0 {
		return true
	}
	return false
}

//Get - Get Value of key
func (c *Cache) Get(key string) ([]byte, map[string]string, error) {
	value, err := redisClient.Get(key).Result()
	headers, _ := redisClient3.HGetAll(key).Result()
	return []byte(value), headers, err
}

func (c *Cache) put(key string, header http.Header, content []byte) error {
	err := redisClient.Set(key, content, 0).Err()
	for k, vs := range header {
		for _, v := range vs {
			redisClient3.HSet(key, k, v)
		}
	}
	if err != nil {
		return err
	}
	setExpiration(key)
	return nil
}

func setExpiration(key string) {
	exp, _ := strconv.Atoi(getEnv("CACHE_EXP", "60"))
	err := redisClient2.Set(key, key, time.Duration(exp)*time.Second).Err()
	if err != nil {
		fmt.Printf("Occured an error on set expiration cache: %s", key)
	}
	return
}

//CheckUpdateAll - CheckUpdateAll
func (c *Cache) CheckUpdateAll() {
	for {
		keys := redisClient.Keys("/api/core/*").Val()
		for _, k := range keys {
			c.checkUpdate(k)
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func (c *Cache) checkUpdate(key string) bool {

	total, err := redisClient2.Exists(key).Result()
	if err != nil {
		fmt.Printf("Occured an erro on check valid expiration cache: %s", key)
		return false
	}
	if total > 0 {
		return false
	}
	fmt.Printf("Cache %s is expired, recreating", key)

	//go recreate(key)
	UpdateRedisKey(key)
	return true
}

//CreateRedisKey - CreateRedisKey
func CreateRedisKey(w http.ResponseWriter, r *http.Request, fullURL string) (string, error) {

	//time.Sleep(1 * time.Second)
	response, err := gatewayRequest(fullURL, r)
	if err != nil {
		handleError(err, w)
		return "", nil
	}

	body, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		handleError(err, w)
		return "", nil
	}

	fmt.Printf("Creating cache for %s", fullURL)
	err = CacheRedis.put(fullURL, r.Header, body) //response.Header, body)

	if err != nil {
		fmt.Printf("Could not write into cache: %s\n", err)
		return "", nil
	}

	copyHeaders(w.Header(), response.Header)
	w.Header().Set("Cache-Status", "MISS")
	_, _ = w.Write(body)

	return string(body), err
}

//UpdateRedisKey - UpdateRedisKey
func UpdateRedisKey(key string) {

	v, err, shared := requestGroup.Do(key, func() (interface{}, error) {

		headers, _ := redisClient3.HGetAll(key).Result()
		reqHeader, response, err := gatewayRequestUpdate(key, headers)
		if response == nil {
			fmt.Printf("Response is nil")
			return "", err
		}
		body, err := ioutil.ReadAll(response.Body)
		defer response.Body.Close()
		if body == nil {
			fmt.Printf("Body Response is null")
			return "", err
		}
		if err != nil {
			fmt.Printf("Occured error on update cache: %s - Error: %v", key, err)
			return "", err
		}
		err = CacheRedis.put(key, reqHeader, body)

		if err != nil {
			fmt.Printf("Occured error on update cache: %s - Error: %v", key, err)
			return "", err
		}

		return response.Status, err
	})
	if err != nil {
		return
	}

	status := v.(string)

	fmt.Printf("UpdateRedisKey handler request: status %q, shared result %t", status, shared)

}

func recreate(key string) {
	fmt.Printf("Updating cache from %s", key)
	setExpiration(key)

	headers, _ := redisClient3.HGetAll(key).Result()

	reqHeader, response, err := gatewayRequestUpdate(key, headers)

	if response == nil {
		fmt.Printf("Response is nil")
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if body == nil {
		fmt.Printf("Body Response is null")
		return
	}
	if err != nil {
		fmt.Printf("Occured error on update cache: %s - Error: %v", key, err)
		return
	}
	//err = CacheRedis.put(key, response.Header, body)
	err = CacheRedis.put(key, reqHeader, body)

	if err != nil {
		fmt.Printf("Occured error on update cache: %s - Error: %v", key, err)
		return
	}
}

//DeleteAllKeys - DeleteAllKeys
func (c *Cache) DeleteAllKeys() {
	fmt.Printf("Delete All Keys")
	redisClient3.FlushAll()
	redisClient.FlushAll()
	redisClient2.FlushAll()
}

func (c *Cache) refreshKey(key string) {
	go recreate(key)
}

func (c *Cache) getAllKeys(key string) []string {
	return redisClient.Keys(key).Val()
}

func (c *Cache) deleteKey(key string) {
	redisClient.Del(key)
	redisClient2.Del(key)
	redisClient3.Del(key)
}
