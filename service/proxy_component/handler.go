package service

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo"
	"golang.org/x/sync/singleflight"
)

// Proxy - Proxy Struct
type Proxy struct{}

var requestGroup singleflight.Group

//FlagDeleteKey - FlagDeleteKey
var FlagDeleteKey bool = false

// Client - HTTP Client Variable
var Client *http.Client

// CacheRedis - Redis Cache Variable
var CacheRedis *Cache

//RenewCache - RenewCache
func RenewCache(w http.ResponseWriter, r *http.Request, fullURL string) {

	response, err := gatewayRequest(fullURL, r)
	if err != nil {
		handleError(err, w)
		return
	}

	body, err := ioutil.ReadAll(response.Body)
	//_ = response.Body.Close()
	defer response.Body.Close()
	if err != nil {
		handleError(err, w)
		return
	}

	fmt.Printf("Creating cache for %s", fullURL)
	err = CacheRedis.put(fullURL, r.Header, body) //response.Header, body)

	if err != nil {
		fmt.Printf("Could not write into cache: %s\n", err)
	}

	copyHeaders(w.Header(), response.Header)
	w.Header().Set("Cache-Status", "MISS")
	_, _ = w.Write(body)
}

//UseCache - UseCache
func UseCache(w http.ResponseWriter, r *http.Request, fullURL string) {
	content, headers, err := CacheRedis.Get(fullURL)

	var status string
	//update := CacheRedis.checkUpdate(fullURL)
	update := false
	if update {
		status = "UPDATING"
	} else {
		status = "HIT"
	}

	if err != nil {
		handleError(err, w)
	} else {
		for key, value := range headers {
			w.Header().Set(key, value)
		}
		w.Header().Set("Cache-Status", status)
		// v := string(content)
		// fmt.Info.Println(v)
		_, _ = w.Write(content)
		return
	}
}

//ForwardRequest - ForwardRequest
func ForwardRequest(w http.ResponseWriter, r *http.Request, fullURL string) {
	// retCode := orimxRealtimeHandler(fullURL)
	// if retCode != 0 {

	// 	return
	// }
	response, err := gatewayRequest(fullURL, r)
	if err != nil {
		handleError(err, w)
		return
	}

	body, err := ioutil.ReadAll(response.Body)
	//_ = response.Body.Close()
	defer response.Body.Close()
	if err != nil {
		handleError(err, w)
		return
	}

	if err != nil {
		fmt.Printf("Could not write into cache: %s\n", err)
	}

	copyHeaders(w.Header(), response.Header)
	_, _ = w.Write(body)
}

func gatewayRequest(fullURL string, r *http.Request) (*http.Response, error) {

	URL := "http://localhost:1323" + fullURL
	req, _ := http.NewRequest(r.Method, URL, r.Body)
	copyHeaders(req.Header, r.Header)
	//fmt.Info.Println(req.Header.Get("token"))

	response, err := Client.Do(req)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func gatewayRequestUpdate(fullURL string, headers map[string]string) (http.Header, *http.Response, error) {

	URL := "http://localhost:1323" + fullURL
	req, _ := http.NewRequest("GET", URL, nil)
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	response, err := Client.Do(req)
	if err != nil {
		return nil, nil, err
	}
	return req.Header, response, nil
}

func copyHeaders(dst, src http.Header) {
	for k := range dst {
		dst.Del(k)
	}
	for k, vs := range src {
		for _, v := range vs {
			dst.Add(k, v)
		}
	}
}

func handleError(err error, w http.ResponseWriter) {
	fmt.Printf(err.Error())
	w.WriteHeader(500)
	_, _ = fmt.Fprintf(w, err.Error())
}

//HandleRequest - HandleRequest
func (p *Proxy) HandleRequest(c echo.Context) {
	w := c.Response().Writer
	r := c.Request()

	fullURL := r.URL.Path + "?" + r.URL.RawQuery
	fmt.Printf("Requested '%s' '%s'\n", r.Method, fullURL)

	if r.Method != "GET" {
		ForwardRequest(w, r, fullURL)
	} else {
		if CacheRedis.Has(fullURL) {
			for {
				if !FlagDeleteKey {
					break
				}
			}
			UseCache(w, r, fullURL)
		} else {
			keyDedupe := r.URL.Path
			p.DedupingHandle(keyDedupe, fullURL, w, r)
		}
	}
}

//DedupingHandle - DedupingHandle
func (p *Proxy) DedupingHandle(dedupeKey, fullURL string, w http.ResponseWriter, r *http.Request) {
	_, err, shared := requestGroup.Do(dedupeKey, func() (interface{}, error) {
		CreateRedisKey(w, r, fullURL)
		return "", nil
	})

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("CreateRedisKey handler request: shared result %t", shared)
	fmt.Fprintf(w, "")
}
