package service

import (
	"fmt"
	"strings"
	"time"
)

var keys []string

func orimxRealtimeHandler(fullURL string) int {
	if strings.Contains(fullURL, "/api/core/v1/records/create") {
		fmt.Printf("Refresh Keys")
		ResetKey("/api/core/v1/*")
	}
	return 0
}

//ResetKey - ResetKey
func ResetKey(keyWild string) {
	keys = CacheRedis.getAllKeys(keyWild)
	for _, key := range keys {
		fmt.Println("Refresh " + key)
		CacheRedis.deleteKey(key)
	}
}

//CheckTokenExpireInKey - CheckTokenExpireInKey
func CheckTokenExpireInKey() {
	for {
		removeKeyHasValueTokenExpired("/api/core/v1/*")
		//removeKeyHasValueTokenExpired("/api/core/templistissues/*")
		time.Sleep(10 * time.Second)
	}
}

func removeKeyHasValueTokenExpired(keyWild string) {
	key := CacheRedis.getAllKeys(keyWild)
	for _, k := range key {
		content, _, err := CacheRedis.Get(k)
		if err != nil {
			fmt.Println(err)
		} else {
			if strings.Contains(string(content), "Token expired") {
				fmt.Println(k + " has value: Token Expired")
				CacheRedis.deleteKey(k)
			}
		}
	}
}
