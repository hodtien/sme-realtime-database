package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"sort"
	"strings"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"github.com/labstack/echo"
	"github.com/metakeule/fmtdate"
)

// PushDataToQueue - Push a Data To Queue
func PushDataToQueue(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	queueName := c.Param("queue")

	// Read the Body content
	bodyBytes, err := ioutil.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
	}
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	_, err = db.LPush("QM"+"$"+userID+"$"+deviceID+"$"+queueName, string(bodyBytes))
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Push Data to Queue Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// PopDataFromQueue - Pop a Data From Queue
func PopDataFromQueue(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	queueName := c.Param("queue")

	retData, err := db.LPop("QM" + "$" + userID + "$" + deviceID + "$" + queueName)
	if retData == "" {
		return c.JSON(200, map[string]interface{}{"code": "0", "data": nil})
	}
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Pop Data from Queue Failed"})
	}
	var data interface{}
	err = json.Unmarshal([]byte(retData), &data)
	if err == nil {
		// Dữ liệu trả về là JSON => Parse & Return
		return c.JSON(200, map[string]interface{}{"code": "0", "data": data})
	}

	// Dữ liệu trả về là Text => Kiểm tra trong DB 0 (dạng Set bình thường) có dữ liệu không
	contentDataDateTime, err := db.Get("QM" + "_" + retData)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Pop Data from Queue Failed"})
	}

	// Kiểm tra trong DB 0 (dạng Set bình thường) không có dữ liệu => Trả về những gì nhận được đã Pop từ Queue
	err = json.Unmarshal([]byte(contentDataDateTime), &data)
	if err != nil {
		return c.JSON(200, map[string]interface{}{"code": "0", "data": contentDataDateTime})
	}

	// Ngược lại trả về content được lưu trong Set của DB 0
	return c.JSON(200, map[string]interface{}{"code": "0", "data": data})

}

// ListDataInQueue - List All Data In Queue
func ListDataInQueue(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	queueName := c.Param("queue")
	retData, err := db.LRange("QM" + "$" + userID + "$" + deviceID + "$" + queueName)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Get All Data in Queue Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "data": retData})
}

// PushDataByDateTimeToQueue - Push a Data By DateTime Field in JSON Body to Queue
func PushDataByDateTimeToQueue(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	queueName := c.Param("queue")

	// Read the Body content
	bodyBytes, err := ioutil.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		fmt.Println("ERROR: ", err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
	}
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	data := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		fmt.Println(err)
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "Body Data is Invalid"})
	}
	dateTime := data["date_time"].(string)
	_, err = fmtdate.Parse("YYYY-MM-DD_hh:mm:ss", dateTime)
	if err != nil {
		fmt.Println(err)
	}

	_, err = db.LPush("datetime"+"$"+"QM"+"$"+userID+"$"+deviceID+"$"+queueName, dateTime)
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Push Data to Queue By DateTime Failed"})
	}
	dataByte, err := json.Marshal(data)
	_, err = db.Set("QM_"+dateTime, string(dataByte))
	if err != nil {
		fmt.Println(err)
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Push Data to Queue By DateTime Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "message": "OK"})
}

// SortDataByDateTimeInQueue - Sort All Data In Queue By Date Time
func SortDataByDateTimeInQueue(c echo.Context) error {
	apikey := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]
	userID, deviceID, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
	if deviceID == "" || userID == "" || err != nil {
		return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
	}
	queueName := c.Param("queue")
	redisKey := "QM" + "$" + userID + "$" + deviceID + "$" + queueName
	listDateTimeInQueue, err := db.LRange(redisKey)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Sort Data By DateTime in Queue Failed"})
	}
	if len(listDateTimeInQueue) == 0 {
		return c.JSON(200, map[string]interface{}{"code": "1", "message": "Queue " + queueName + " is empty"})
	}
	sort.Strings(listDateTimeInQueue)
	_, err = db.LDel(redisKey)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Sort Data By DateTime in Queue Failed"})
	}
	_, err = db.RPush(redisKey, listDateTimeInQueue)
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": "-1", "message": "Sort Data By DateTime in Queue Failed"})
	}
	return c.JSON(200, map[string]interface{}{"code": "0", "data": listDateTimeInQueue})
}
