#!/bin/bash
# Auto re-deploy project script
rancherServers=(
'dev|https://orimx-rancher-demo:8443/v3/project/c-jzttc:p-4dcwj/workload/deployment:vnpt-business-platform:sme-realtime-database-test|token-xnwps:hqbc88hk2m2xs227vczvnc842pjgmkg46lbfmkmwzf9ksj8rtlnqfk'
)
branch="$1"
if [ -z $1 ]; then
	echo "Specify branch empty, no branch to deploy!"
else
	for i in "${rancherServers[@]}";do
		dBranch=$(echo $i | cut -d'|' -f1)
		rancherUrl=$(echo $i | cut -d'|' -f2)
		token=$(echo $i | cut -d'|' -f3)
		if [ $dBranch = $branch ]; then
			echo "Deploy branch $branch at $rancherUrl"
			data="{\"annotations\":{\"cattle.io/timestamp\":\"$(date -u +'%Y-%m-%dT%H:%M:%SZ')\"}}"
			curl -ik -X PUT $rancherUrl -H "Authorization: Bearer $token" -H 'content-type: application/json' -d "$data"
			exit
		fi
	done
	echo "Branch $branch was not define rancher server to deploy!"
fi

#test auto