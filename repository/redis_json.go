package repository

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"

	"github.com/KromDaniel/jonson"
	"github.com/nitishm/go-rejson"
)

//Redis JSON v1: docker run -d -p 2222:6379 redislabs/rejson:latest
//Redis JSON v2: docker run -p 2222:6379 -d redislabs/redisjson2:0.8.0

var wg sync.WaitGroup

//ReJSONUpdate - JSON Redis Driver Update Data
func (c *Cache) ReJSONUpdate(key, path, value string) (string, error) {
	str, err := redisJSONWrite0.JsonSet(key, path, value).Result()
	if err != nil {
		fmt.Println(err)
		return str, err
	}
	return str, err
}

//ReJSONSet - JSON Redis Driver Set Data
func (c *Cache) ReJSONSet(key, path, value string, args interface{}) (string, error) {
	str, err := redisJSONWrite0.JsonSet(key, path, value, args).Result()
	if err != nil {
		fmt.Println(err)
		return str, err
	}
	return str, err
}

//ReJSONGet - JSON Redis Driver Get a Data
func (c *Cache) ReJSONGet(key string, query string) (interface{}, error) {
	//Handling JSON Get with redis json v1
	// jsonString, err := client.JsonGet(key, "NOESCAPE", query).Result()
	// return Decode(jsonString)

	//Handing JSON Get with redis json v2
	if query == "" {
		query = "."
	}
	var args []interface{}
	args = append(args, "NOESCAPE")
	args = append(args, query)
	jsonString, err := redisJSONRead0.JsonGet(key, args...).Result()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var m interface{}
	err = json.Unmarshal([]byte(jsonString), &m)
	if err != nil {
		fmt.Println(err.Error())
	}
	return m, nil
}

func hashGetAllRoutine(jsonGetAllChan chan map[string]interface{}, waitgroup *sync.WaitGroup, jstring interface{}, key string, opts ...string) {
	defer waitgroup.Done()
	if jstring == nil {
		return
	}
	var getForType string = opts[0]
	var respData map[string]interface{}
	var m interface{}
	err := json.Unmarshal([]byte(jstring.(string)), &m)
	if err != nil {
		fmt.Println(err.Error())
	}

	switch getForType {
	case "device":
		respData = map[string]interface{}{
			"device_id": strings.Split(key, "$")[2],
			"data_item": m,
		}
		break
	case "bucket":
		if opts[1] == "" {
			return
		}
		respData = map[string]interface{}{
			"bucket_id": strings.Split(key, "$")[3],
			"device_id": strings.Split(key, "$")[2],
			"data_item": m,
		}
		break
	case "bucket_all":
		respData = map[string]interface{}{
			"bucket_id": strings.Split(key, "$")[3],
			"device_id": strings.Split(key, "$")[2],
			"data_item": m,
		}
		break
	case "record":
		if opts[1] == "" && opts[2] == "" {
			return
		}
		respData = map[string]interface{}{
			"record_id": strings.Split(key, "$")[3],
			"bucket_id": strings.Split(key, "$")[2],
			"device_id": strings.Split(key, "$")[1],
			"data_item": m,
		}
		break
	case "record_all":
		respData = map[string]interface{}{
			"record_id": strings.Split(key, "$")[3],
			"bucket_id": strings.Split(key, "$")[2],
			"device_id": strings.Split(key, "$")[1],
			"data_item": m,
		}
		break
	case "minio_record_all":
		respData = map[string]interface{}{
			"record_id": strings.Split(key, "$")[4],
			"bucket_id": strings.Split(key, "$")[3],
			"device_id": strings.Split(key, "$")[2],
			"data_item": m,
		}
	case "minio_bucket_all":
		respData = map[string]interface{}{
			"bucket_id": strings.Split(key, "$")[4],
			"device_id": strings.Split(key, "$")[3],
			"data_item": m,
		}
		break
	default:
		respData = map[string]interface{}{
			"data_item": m,
		}
		break
	}
	jsonGetAllChan <- respData
}

//HashGetAll - JSON Redis Driver Get All Data
func (c *Cache) HashGetAll(bucket, query string, keys []string, opts ...string) (interface{}, error) {
	var waitgroup sync.WaitGroup
	var strArr []interface{}
	var resp responseAll

	jsonString, err := redisClientRead0.HMGet("alldata1", keys...).Result()
	if err != nil {
		fmt.Println(err.Error())
	}
	queue := make(chan map[string]interface{}, len(jsonString))
	for i, jstring := range jsonString {
		waitgroup.Add(1)
		go hashGetAllRoutine(queue, &waitgroup, jstring, keys[i], opts...)

	}
	waitgroup.Wait()
	close(queue)
	for elem := range queue {
		strArr = append(strArr, elem)
	}

	if strArr == nil {
		return nil, nil
	}
	resp.Data = strArr
	return resp, nil
}

//ReJSONGetAllInBucket - JSON Redis Driver Get All Data in Bucket
func (c *Cache) ReJSONGetAllInBucket(bucket, query string, keys []string, opts ...string) (interface{}, error) {
	var waitgroup sync.WaitGroup
	var strArr []interface{}
	var resp responseAll
	var args []interface{}

	args = append(args, "NOESCAPE")
	args = append(args, query)

	var jsonString []string
	queue := make(chan string, len(keys))
	for _, k := range keys {
		waitgroup.Add(1)
		go func(k string) {
			defer waitgroup.Done()
			v, err := redisJSONRead0.JsonGet(k, args...).Result()
			if err != nil {
				fmt.Println(err.Error())
			}
			queue <- v
		}(k)
	}
	waitgroup.Wait()
	close(queue)
	for elem := range queue {
		jsonString = append(jsonString, elem)
	}

	var respData map[string]interface{}
	var getForType string = opts[0]

	for i, jstring := range jsonString {
		if jstring == "" {
			continue
		}
		var m interface{}
		j, err := jonson.Parse([]byte(jstring))
		if err != nil {
			fmt.Println(err.Error())
		}
		m = j.ToInterface()

		switch getForType {
		case "device":
			respData = map[string]interface{}{
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
			break
		case "bucket":
			if opts[1] == "" {
				return nil, nil
			}
			respData = map[string]interface{}{
				"bucket_id": strings.Split(keys[i], "$")[3],
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
			break
		case "bucket_all":
			respData = map[string]interface{}{
				"bucket_id": strings.Split(keys[i], "$")[3],
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
			break
		case "record":
			if opts[1] == "" && opts[2] == "" {
				return nil, nil
			}
			respData = map[string]interface{}{
				"record_id": strings.Split(keys[i], "$")[3],
				"bucket_id": strings.Split(keys[i], "$")[2],
				"device_id": strings.Split(keys[i], "$")[1],
				"data_item": m,
			}
			break
		case "record_all":
			respData = map[string]interface{}{
				"record_id": strings.Split(keys[i], "$")[3],
				"bucket_id": strings.Split(keys[i], "$")[2],
				"device_id": strings.Split(keys[i], "$")[1],
				"data_item": m,
			}
			break
		case "minio_record_all":
			respData = map[string]interface{}{
				"record_id": strings.Split(keys[i], "$")[4],
				"bucket_id": strings.Split(keys[i], "$")[3],
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
		case "minio_bucket_all":
			respData = map[string]interface{}{
				"bucket_id": strings.Split(keys[i], "$")[4],
				"device_id": strings.Split(keys[i], "$")[3],
				"data_item": m,
			}
			break
		default:
			respData = map[string]interface{}{
				"data_item": m,
			}
			break
		}

		strArr = append(strArr, respData)
	}
	if strArr == nil {
		return nil, nil
	}
	resp.Data = strArr
	return resp, nil
}

//ReJSONGetAll - JSON Redis Driver Get All Data
func (c *Cache) ReJSONGetAll(bucket, query string, keys []string, opts ...string) (interface{}, error) {
	var waitgroup sync.WaitGroup
	var strArr []interface{}
	var resp responseAll

	var args []interface{}
	args = append(args, "NOESCAPE")
	args = append(args, query)

	var jsonString []string
	queue := make(chan string, len(keys))
	for _, k := range keys {
		waitgroup.Add(1)
		go func(k string) {
			defer waitgroup.Done()
			v, err := redisJSONRead0.JsonGet(k, args...).Result()
			if err != nil {
				fmt.Println(err.Error())
			}
			queue <- v
		}(k)
	}
	waitgroup.Wait()
	close(queue)
	for elem := range queue {
		jsonString = append(jsonString, elem)
	}

	var respData map[string]interface{}
	var getForType string = opts[0]

	for i, jstring := range jsonString {
		if jstring == "" {
			continue
		}
		var m interface{}
		j, err := jonson.Parse([]byte(jstring))
		if err != nil {
			fmt.Println(err.Error())
		}
		m = j.ToInterface()

		switch getForType {
		case "device":
			respData = map[string]interface{}{
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
			break
		case "bucket":
			if opts[1] == "" {
				return nil, nil
			}
			respData = map[string]interface{}{
				"bucket_id": strings.Split(keys[i], "$")[3],
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
			break
		case "bucket_all":
			respData = map[string]interface{}{
				"bucket_id": strings.Split(keys[i], "$")[3],
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
			break
		case "record":
			if opts[1] == "" && opts[2] == "" {
				return nil, nil
			}
			respData = map[string]interface{}{
				"record_id": strings.Split(keys[i], "$")[3],
				"bucket_id": strings.Split(keys[i], "$")[2],
				"device_id": strings.Split(keys[i], "$")[1],
				"data_item": m,
			}
			break
		case "record_all":
			respData = map[string]interface{}{
				"record_id": strings.Split(keys[i], "$")[3],
				"bucket_id": strings.Split(keys[i], "$")[2],
				"device_id": strings.Split(keys[i], "$")[1],
				"data_item": m,
			}
			break
		case "minio_record_all":
			respData = map[string]interface{}{
				"record_id": strings.Split(keys[i], "$")[4],
				"bucket_id": strings.Split(keys[i], "$")[3],
				"device_id": strings.Split(keys[i], "$")[2],
				"data_item": m,
			}
		case "minio_bucket_all":
			respData = map[string]interface{}{
				"bucket_id": strings.Split(keys[i], "$")[4],
				"device_id": strings.Split(keys[i], "$")[3],
				"data_item": m,
			}
			break
		default:
			respData = map[string]interface{}{
				"data_item": m,
			}
			break
		}

		strArr = append(strArr, respData)
	}
	if strArr == nil {
		return nil, nil
	}
	resp.Data = strArr
	return resp, nil
}

// ReJSONGetAnyAll - ReJSONGetAnyAll
func (c *Cache) ReJSONGetAnyAll(keys []string, query string, opts ...string) (interface{}, error) {
	firstKey := keys[0]
	var strArr []interface{}
	var args []interface{}

	for _, key := range keys[1:] {
		args = append(args, key)
	}

	args = append(args, "NOESCAPE")
	args = append(args, query)
	jsonString, err := redisJSONRead0.JsonMGet(firstKey, args...).Result()
	if err != nil {
		fmt.Println(err.Error())
		//return nil, err
	}
	for _, jstring := range jsonString {
		if jstring == "" {
			continue
		}
		var m interface{}
		err = json.Unmarshal([]byte(jstring), &m)
		if err != nil {
			fmt.Println(err.Error())
		}
		strArr = append(strArr, m)
	}

	return strArr, nil
}

//ReJSONArrLen - JSON Redis Driver Get Array Length
func (c *Cache) ReJSONArrLen(key, query string) (interface{}, error) {
	rh := rejson.NewReJSONHandler()
	rh.SetGoRedisClient(redisClientRead0)
	len, err := rh.JSONArrLen(key, query)
	return len, err
}

//Delete - Delete a Record
func (c *Cache) Delete(key string) error {
	err := redisClientWrite0.Del(key).Err()
	return err
}

//DeleteAll - DeleteAll
func (c *Cache) DeleteAll(key string) {
	keys := redisClientRead0.Keys(key + "*").Val()
	for _, key := range keys {
		redisClientWrite0.Del(key)
	}
}

//RealtimeNotification - Push Notification to Redis Hub
func (c *Cache) RealtimeNotification(channel string, msg interface{}) error {
	err := redisClientWrite0.Publish(channel, msg).Err()
	defer redisClientWrite0.Close()
	return err
}

//Decode - Decode
func Decode(jsonString string) (interface{}, error) {
	rejson, err := jonson.Parse([]byte(jsonString))
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	ret, err := rejson.ToJSONString()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var m interface{}
	err = json.Unmarshal([]byte(ret), &m)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return m, nil
}

//ReJSONGetByQuery - JSON Redis Driver Get Records By Query
func (c *Cache) ReJSONGetByQuery(key, query string) []interface{} {
	i := 0
	keys := redisClientRead0.Keys(key + "*").Val()
	var result []interface{}
	for _, k := range keys {
		wg.Add(1)
		go func(key string) {
			ret, err := redisJSONRead0.JsonGet(key, "NOESCAPE", query).Result()
			if err == nil {
				m, err := Decode(ret)
				if err != nil {
					fmt.Println(err)
				}
				result = append(result, m)
				i++
			}
			wg.Done()
		}(k)
	}
	wg.Wait()
	fmt.Println(i)

	return result
}
