package repository

import (
	"time"

	"github.com/KromDaniel/rejonson"
	"github.com/go-redis/redis"
)

// CreateCache - Create Redis Connection
func (c *Cache) CreateCache(redisHost string, maxClients, minIdle int) {
	RedisHost = redisHost
	redisClientRead1 = redis.NewClient(&redis.Options{
		Addr:         RedisHost,
		Password:     "",
		DB:           1,
		PoolSize:     maxClients,
		PoolTimeout:  30 * time.Second,
		MinIdleConns: minIdle,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		DialTimeout:  1 * time.Minute,
		MaxRetries:   3,
	})
	redisClientWrite1 = redis.NewClient(&redis.Options{
		Addr:         RedisHost,
		Password:     "",
		DB:           1,
		PoolSize:     maxClients,
		PoolTimeout:  30 * time.Second,
		MinIdleConns: minIdle,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		DialTimeout:  1 * time.Minute,
		MaxRetries:   3,
	})

	redisClientRead0 = redis.NewClient(&redis.Options{
		Addr:         RedisHost,
		Password:     "",
		DB:           0,
		PoolSize:     maxClients,
		PoolTimeout:  30 * time.Second,
		MinIdleConns: minIdle,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		DialTimeout:  1 * time.Minute,
		MaxRetries:   3,
	})
	redisClientWrite0 = redis.NewClient(&redis.Options{
		Addr:         RedisHost,
		Password:     "",
		DB:           0,
		PoolSize:     maxClients,
		PoolTimeout:  30 * time.Second,
		MinIdleConns: minIdle,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		DialTimeout:  1 * time.Minute,
		MaxRetries:   3,
	})
	redisJSONRead0 = rejonson.ExtendClient(redisClientRead0)
	redisJSONWrite0 = rejonson.ExtendClient(redisClientWrite0)
}

// Close - Close
func (c *Cache) Close() {
	redisClientRead0.Close()
	redisClientWrite0.Close()
	redisClientRead1.Close()
	redisClientWrite1.Close()
	redisJSONRead0.Close()
	redisJSONWrite0.Close()
}

/* Đánh index cho record để phân trang dữ liệu, tham khảo: https://christophermcdowell.dev/post/pagination-with-redis */

// GetKeysByIndex - GetKeysByIndex
func (c *Cache) GetKeysByIndex(key string, start, stop int64) ([]string, error) {
	return c.ZRange(1, key, start, stop)
}

//SetIndex - SetIndex
func (c *Cache) SetIndex(key, member string) error {
	timestamp := []float64{float64(time.Now().Local().Unix())}
	members := []string{member}
	_, err := c.ZAdd(1, key, timestamp, members)
	return err
}

// RemoveIndex - RemoveIndex
func (c *Cache) RemoveIndex(key, member string) error {
	_, err := c.ZRem(1, key, member)
	return err
}

/***********************************/
