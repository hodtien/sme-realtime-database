package repository

import (
	"os"
	"time"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// Save - Save data to Persistent Volume
func (c *Cache) Save() {
	redisClientWrite0.Save()
}

//Keys - Get all Keys with pattern
func (c *Cache) Keys(key string) []string {
	keys := redisClientRead0.Keys(key + "*").Val()
	return keys
}

//Set - Set a value with key to Redis DB
func (c *Cache) Set(key string, value interface{}) (string, error) {
	ret, err := redisClientWrite0.Set(key, value, 0).Result()
	return ret, err
}

//SetExpire - Set a value with key to Redis DB with Expire Time
func (c *Cache) SetExpire(key string, value interface{}, t time.Duration) (string, error) {
	ret, err := redisClientWrite0.Set(key, value, t).Result()
	return ret, err
}

//SetNX - Set a value with key to Redis DB (when key is not exists)
func (c *Cache) SetNX(key string, value interface{}, expTime time.Duration) (bool, error) {
	ret, err := redisClientWrite0.SetNX(key, value, expTime).Result()
	return ret, err
}

//HSet - Put dữ liệu Hash ứng với 1 Key
func (c *Cache) HSet(key, field string, value interface{}) error {
	_, err := redisClientWrite0.HSet(key, field, value).Result()
	return err
}

//HGet - Get dữ liệu Hash ứng với 1 Key
func (c *Cache) HGet(key, field string) (string, error) {
	ret, err := redisClientWrite0.HGet(key, field).Result()
	return ret, err
}

//HExists - Check Field cua Hash co ton tai khong
func (c *Cache) HExists(key, field string) bool {
	ret, _ := redisClientWrite0.HExists(key, field).Result()
	return ret
}

//HDel - Delete một hoặc nhiều dữ liệu trong Hash với Key tương ứng
func (c *Cache) HDel(key string, fields ...string) (int64, error) {
	ret, err := redisClientWrite0.HDel(key, fields...).Result()
	return ret, err
}

//HMSet - Put nhiều dữ liệu Hash ứng với 1 Key
func (c *Cache) HMSet(key string, fields map[string]interface{}) error {
	_, err := redisClientWrite0.HMSet(key, fields).Result()
	return err
}

//HMGet - Get nhiều dữ liệu Hash ứng với 1 Key
func (c *Cache) HMGet(key string, fields ...string) ([]interface{}, error) {
	ret, err := redisClientRead0.HMGet(key, fields...).Result()
	return ret, err
}

//Get - Get dữ liệu ứng với 1 Key
func (c *Cache) Get(key string) (string, error) {
	ret, err := redisClientRead0.Get(key).Result()
	return ret, err
}

//Exists - Kiểm tra nếu 1 key tồn tại trong DB
func (c *Cache) Exists(key string) bool {
	ret := redisClientRead0.Exists(key).Val()
	if ret == 0 {
		return false
	}
	return true
}

//StoragePersistent - Storage Data to Disk
func (c *Cache) StoragePersistent() (err error) {
	err = redisClientWrite0.Save().Err()
	if err != nil {
		return err
	}
	err = redisClientWrite0.Save().Err()
	if err != nil {
		return err
	}
	return nil
}
