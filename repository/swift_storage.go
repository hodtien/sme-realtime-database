package repository

import (
	"fmt"
	"log"
	"time"

	"github.com/ncw/swift"
	"github.com/spf13/viper"
)

//CreateContainer - Tạo mới một Container
func (c *Cache) CreateContainer(containerName string) error {
	containerHeader := swift.Headers{
		"X-Container-Read": ".r:*,.rlistings",
	}
	err := SwiftConnection.ContainerCreate(containerName, containerHeader)
	return err
}

//UploadToSwift - Upload File lên Swift Storage
func (c *Cache) UploadToSwift(container string, fileName string, buf []byte, contentType string) {
	// upload file to swift until success
	log.Println("Begin upload file to swift: " + fileName)
	err := SwiftConnection.ObjectPutBytes(container, fileName, buf, contentType)

	// reupload file to swift if upload fail at the one before
	if err != nil {
		time.Sleep(3 * time.Second)
		log.Println("Failed to upload file to swift. File name:", fileName, " --> Begining reupload...")
		errorReupload := SwiftConnection.ObjectPutBytes(container, fileName, buf, contentType)
		if errorReupload != nil {
			log.Println("Failed to Reupload file to swift. File name:", fileName, " -- detail error:", errorReupload)
		}
	} else {
		log.Println("Upload file to swift succeeded: " + fileName)
	}
}

//DeleteFile - Delete File From Swift Storage Driver
func (c *Cache) DeleteFile(container, fileName string) error {
	err := SwiftConnection.ObjectDelete(container, fileName)
	return err
}

//DeleteContainer - Delete Container From Swift Storage Driver
func (c *Cache) DeleteContainer(container string) error {
	objectNames, err := SwiftConnection.ObjectNames(container, nil)
	if err != nil {
		return err
	}
	_, err = SwiftConnection.BulkDelete(container, objectNames)
	if err != nil {
		return err
	}
	err = SwiftConnection.ContainerDelete(container)
	return err
}

//SwiftAuthenticate - Authenticate VNPT Swift Storage
func (c *Cache) SwiftAuthenticate() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	SwiftConnection = &swift.Connection{
		UserName:    viper.GetString(`swift.username`),
		ApiKey:      viper.GetString(`swift.password`),
		AuthUrl:     viper.GetString(`swift.url.auth`),
		Domain:      viper.GetString(`swift.domain`),
		Tenant:      viper.GetString(`swift.tenant`),
		AuthVersion: 3,
	}

	// Authenticate
	err = SwiftConnection.Authenticate()
	if err != nil {
		panic(err)
	}
	SwiftConnection.StorageUrl = viper.GetString(`swift.url.storage`)
	fmt.Println("Swift Authentication Token: ", SwiftConnection.AuthToken)
}

//ReAuthenticate - ReAuthenticate VNPT Swift Storage
func (c *Cache) ReAuthenticate() {
	fmt.Println("=== Thread Check Authentication Started ===")
	for {
		expireToken := SwiftConnection.Expires
		loc, _ := time.LoadLocation("UTC")
		currentTime := time.Now().In(loc)
		//currentTime = currentTime.Add(1 * time.Hour)
		diff := expireToken.Sub(currentTime)
		if diff.Hours() < 0.98 {
			fmt.Println(diff.Hours())
			fmt.Println("=== ReAuthenticate VNPT Swift Storage ===")
			c.SwiftAuthenticate()
			fmt.Println("*** New Token: ", SwiftConnection.AuthToken)
		}
		time.Sleep(30 * time.Second)
	}
}

//GetSwiftToken - Get Swift Token
func (c *Cache) GetSwiftToken() string {
	return SwiftConnection.AuthToken
}

//GetSwiftStorageURL - Get Swift Storage URL
func (c *Cache) GetSwiftStorageURL() string {
	return SwiftConnection.StorageUrl
}
