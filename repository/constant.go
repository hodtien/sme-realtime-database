package repository

import (
	"github.com/KromDaniel/rejonson"
	"github.com/go-redis/redis"
	"github.com/ncw/swift"
	"gopkg.in/mgo.v2"
)

type responseAll struct {
	Data []interface{} `json:"data"`
}

// Cache - Redis Cache Struct
type Cache struct{}

// Mgo - Mongo DB Struct
type Mgo struct{}

//DBName - DBName
var DBName = "vnpt_business_platform"

//DB - DB
var db *mgo.Session

// var redisClient *redis.Client
// var redisClient2 *redis.Client

// var client *rejonson.Client

var redisClientRead0, redisClientRead1 *redis.Client
var redisClientWrite0, redisClientWrite1 *redis.Client

var redisJSONRead0 *rejonson.Client
var redisJSONWrite0 *rejonson.Client

//SwiftConnection - SwiftConnection Global Variable
var SwiftConnection *swift.Connection

//Limit - Limit
const Limit = 5

//TotalDocsNum - TotalDocsNum
const TotalDocsNum = 1000

// RedisHost - Redis DB URL
var RedisHost string //= "221.132.29.81:2222"

// MongoHost - Mongo DB URL
var MongoHost string //= "mongodb://221.132.29.81:27018"
