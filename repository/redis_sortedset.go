package repository

import "github.com/go-redis/redis"

/*---------------------------------------------------------------------------------------------*/

//ZAdd - Adds one or more members to a sorted set, or updates its score, if it already exists
func (c *Cache) ZAdd(db int, key string, scores []float64, members []string) (int64, error) {
	var redisZ []*redis.Z
	for i, m := range members {
		redisz := &redis.Z{
			Score:  scores[i],
			Member: m,
		}
		redisZ = append(redisZ, redisz)
	}
	val, err := redisClientWrite1.ZAdd(key, redisZ...).Result()
	return val, err
}

//ZKeys - List All keys of Sorted Set
func (c *Cache) ZKeys(db int, key string) []string {
	keys := redisClientRead1.Keys(key + "*").Val()
	return keys
}

//ZRange - Returns a range of members in a sorted set, by index
func (c *Cache) ZRange(db int, key string, start, stop int64) ([]string, error) {
	ret, err := redisClientRead1.ZRange(key, start, stop).Result()
	return ret, err
}

//ZRangeWithScore - Returns a range of members in a sorted set, by index
func (c *Cache) ZRangeWithScore(db int, key string, start, stop int64) []redis.Z {
	redisZ, _ := redisClientRead1.ZRangeWithScores(key, start, stop).Result()
	return redisZ
}

//ZRank - Determines the index of a member in a sorted set
func (c *Cache) ZRank(db int, key, member string) (int64, error) {
	ret, err := redisClientRead1.ZRank(key, member).Result()
	return ret, err
}

//ZCount - Counts the members in a sorted set with scores within the given values
func (c *Cache) ZCount(db int, key, min, max string) (int64, error) {
	ret, err := redisClientRead1.ZCount(key, min, max).Result()
	return ret, err
}

//ZIncrBy - Increments the score of a member in a sorted set
func (c *Cache) ZIncrBy(db int, key, member string, incr float64) (float64, error) {
	ret, err := redisClientWrite1.ZIncrBy(key, incr, member).Result()
	return ret, err
}

//ZIncr - Increments the score of a member in a sorted set
func (c *Cache) ZIncr(db int, key, member string) (float64, error) {
	redisZ := &redis.Z{Member: member}
	ret, err := redisClientWrite1.ZIncr(key, redisZ).Result()
	return ret, err
}

//ZScore - Gets the score associated with the given member in a sorted set
func (c *Cache) ZScore(db int, key, member string) (float64, error) {
	ret, err := redisClientRead1.ZScore(key, member).Result()
	return ret, err
}

//ZRem - Removes one or more members from a sorted set
func (c *Cache) ZRem(db int, key string, member ...string) (int64, error) {
	ret, err := redisClientWrite1.ZRem(key, member).Result()
	return ret, err
}

//ZDel - Delete a Sorted Set Key
func (c *Cache) ZDel(key string) error {
	err := redisClientWrite1.Del(key).Err()
	return err
}

/*---------------------------------------------------------------------------------------------*/

//ZCard - Gets the number of members in a sorted set
func (c *Cache) ZCard(db int, key string) (int64, error) {
	ret, err := redisClientRead1.ZCard(key).Result()
	return ret, err
}

//ZInterStore - Intersects multiple sorted sets and stores the resulting sorted set in a new key
func (c *Cache) ZInterStore() {}

//ZLexCount - Counts the number of members in a sorted set between a given lexicographical range
func (c *Cache) ZLexCount(key string, min, max int) {}

//ZRangeByLex - Returns a range of members in a sorted set, by lexicographical range
func (c *Cache) ZRangeByLex(key string, start, stop int) {}

//ZRangeByScore - Returns a range of members in a sorted set, by score
func (c *Cache) ZRangeByScore(key string, start, stop int) {}

//ZRemRangeByLex - Removes all members in a sorted set between the given lexicographical range
func (c *Cache) ZRemRangeByLex(key string, min, max int) {}

//ZRemRangeByRank - Removes all members in a sorted set within the given indexes
func (c *Cache) ZRemRangeByRank(key string, start, stop int) {}

//ZRemRangeByScore - Removes all members in a sorted set within the given scores
func (c *Cache) ZRemRangeByScore(key string, min, max int) {}

//ZRevRange - Returns a range of members in a sorted set, by index, with scores ordered from high to low
func (c *Cache) ZRevRange(key string, start, stop int) {}

//ZRevRangeByScore - Returns a range of members in a sorted set, by score, with scores ordered from high to low
func (c *Cache) ZRevRangeByScore(key string, min, max int) {}

//ZRevRank - Determines the index of a member in a sorted set, with scores ordered from high to low
func (c *Cache) ZRevRank(key, member string) {}

//ZUnionStore - Adds multiple sorted sets and stores the resulting sorted set in a new key
func (c *Cache) ZUnionStore(destination, numkeys, key string) {}

//ZScan - Incrementally iterates sorted sets elements and associated scores
func (c *Cache) ZScan(key string, cursor int) {}
