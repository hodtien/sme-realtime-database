package repository

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// InitialDatabase - InitialDatabase
func (c *Mgo) InitialDatabase(mongoURL, mongoDBName string) {
	MongoHost = mongoURL
	DBName = mongoDBName
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:   []string{MongoHost},
		Timeout: 60 * time.Second,
		//PoolLimit: 100000,
	}
	_db, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		fmt.Println("MONGO ERROR: ", err)
	}
	db = _db
}

// Close - Close
func (c *Mgo) Close() {
	db.Close()
}

//FindByID - Find one document in Mongo DB by ID
func (c *Mgo) FindByID(collection string, id string) interface{} {
	var result interface{}
	err := db.DB(DBName).C(collection).Find(bson.M{"id": id}).One(result)

	if err == mgo.ErrNotFound || err != nil {
		return nil
	}
	return result
}

//FindOneMongoByField - Find one document in Mongo DB by any Field
func (c *Mgo) FindOneMongoByField(MongoHost, DBName, collection, f, v string) (interface{}, bool) {
	var result bson.M
	selector := bson.M{f: bson.M{"$regex": v}}
	err := db.DB(DBName).C(collection).Find(selector).One(result)

	if err == mgo.ErrNotFound {
		return nil, false
	}
	return result, true
}

//SaveMongo - Save Data to Mongo DB
func (c *Mgo) SaveMongo(MongoHost, DBName, collection string, ID string, data map[string]interface{}) {
	_, err := db.DB(DBName).C(collection).Upsert(bson.M{"_id": ID}, data)
	if err != nil {
		fmt.Println(err)
	}
}

//UpdateMongo - UpdateMongo
func (c *Mgo) UpdateMongo(data []byte, bucket, key string) {
	value := make(map[string]interface{})
	err := json.Unmarshal(data, &value)
	if err != nil {
		fmt.Println(err)
	}

	filter := bson.M{"id": bson.M{"$eq": key}}
	update := bson.M{"$set": value}

	err = db.DB(DBName).C(bucket).UpdateId(filter, update)
	if err != nil {
		fmt.Println(err)
	}
}

//SearchManyConditionInMongo - Search Data with Many Condition in MongoDB
func (c *Mgo) SearchManyConditionInMongo(bucket string, fields, values []string) (interface{}, bool) {
	var retValue []bson.M
	var andQuery []bson.M
	for i := 0; i < len(fields); i++ {
		selector := bson.M{fields[i]: bson.M{"$regex": values[i]}}
		andQuery = append(andQuery, selector)
	}
	err := db.DB(DBName).C(bucket).Find(bson.M{"$and": andQuery}).All(&retValue)
	if err != nil || len(retValue) == 0 {
		fmt.Println(err)
		return nil, false
	}

	return retValue, true
}

//SearchInMongo - Search Data in MongoDB
func (c *Mgo) SearchInMongo(bucket, field, value string) (interface{}, bool) {
	var retValue []bson.M
	selector := bson.M{field: bson.M{"$regex": value}}
	err := db.DB(DBName).C(bucket).Find(selector).All(&retValue)
	if err != nil || len(retValue) == 0 {
		fmt.Println(err)
		return nil, false
	}

	return retValue, true
}

//DeleteInMongo - Delete Data in MongoDB
func (c *Mgo) DeleteInMongo(bucket, deleteByID string) {
	err := db.DB(DBName).C(bucket).Remove(bson.M{"_id": deleteByID})
	if err != nil {
		fmt.Println(err)
	}
}

//DropCollectionInMongo - Delete a Collection in MongoDB
func (c *Mgo) DropCollectionInMongo(bucket string) {
	err := db.DB(DBName).C(bucket).DropCollection()
	if err != nil {
		fmt.Println(err)
	}
}

//DropManyCollectionInMongo - DropManyCollectionInMongo
func (c *Mgo) DropManyCollectionInMongo(listCollection []string) {
	for _, collection := range listCollection {
		collection = strings.ReplaceAll(collection, "$", "@")
		err := db.DB(DBName).C(collection).DropCollection()
		if err != nil {
			fmt.Println("Error while drop collection ", collection)
		}
	}
}

// FindAllInMongo - FindAllInMongo
func (c *Mgo) FindAllInMongo(collection string) []string {
	var result []bson.M
	var retData []string
	err := db.DB(DBName).C(collection).Find(bson.M{}).All(&result)
	if err != nil {
		fmt.Println(err)
	}
	for _, r := range result {
		rTmp := fmt.Sprintf("%s", r["_id"])
		retData = append(retData, rTmp)
	}
	return retData
}

//SortInMongo - SortInMongo
func (c *Mgo) SortInMongo(collection, sortByField string) (interface{}, error) {

	var result []bson.M
	err := db.DB(DBName).C(collection).Find(bson.M{}).Sort(sortByField).All(&result)
	if err != nil || len(result) == 0 {
		fmt.Println(err)
		return nil, err
	}
	return result, nil
}

//LimitSortInMongo - LimitSortInMongo
func (c *Mgo) LimitSortInMongo(collection, sortByField string, limit int) (interface{}, error) {
	var result []bson.M
	if sortByField == "" {
		err := db.DB(DBName).C(collection).Find(bson.M{}).Limit(limit).All(&result)
		if err != nil || len(result) == 0 {
			fmt.Println(err)
			return nil, err
		}
	} else {
		err := db.DB(DBName).C(collection).Find(bson.M{}).Sort(sortByField).All(&result)
		if err != nil || len(result) == 0 {
			fmt.Println(err)
			return nil, err
		}
	}
	return result, nil
}

//PaginateWithSkip - Phân trang dữ liệu.
// VD: Page 1, Limit 2 => Page 1: record 1, record 2; Page 2: record 3, record 4
//	   Page 2, Limit 3 => Page 1: record 1, record 2, record 3; Page 2: record 4, record 5, record 6
func (c *Mgo) PaginateWithSkip(collection string, page, limit int) ([]interface{}, error) {
	if limit <= 0 {
		return nil, fmt.Errorf("Limit must > 0")
	}

	start := time.Now()
	skip := (page - 1) * limit

	records := make([]interface{}, Limit)
	err := db.DB(DBName).C(collection).Find(nil).Sort("_id").Skip(skip).Limit(limit).All(&records)
	if err != nil || len(records) == 0 {
		fmt.Println(err)
		return nil, err
	}
	rFirstMap := records[0].(bson.M)
	rLastMap := records[len(records)-1].(bson.M)
	rFirst := fmt.Sprintf("%s", rFirstMap["_id"])
	rLast := fmt.Sprintf("%s", rLastMap["_id"])

	fmt.Printf("paginateWithSkip -> page %d with record from %s to %s in %s\n", page, rFirst, rLast, time.Since(start))
	return records, nil
}
