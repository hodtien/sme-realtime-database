package repository

//LPush - Push a Element to Redis List
func (c *Cache) LPush(key string, value interface{}) (int64, error) {
	ret, err := redisClientWrite0.LPush(key, value).Result()
	return ret, err
}

//RPush - Push many Element to Redis List
func (c *Cache) RPush(key string, value ...interface{}) (int64, error) {
	ret, err := redisClientWrite0.RPush(key, value...).Result()
	return ret, err
}

//LPop - Pop a Element from Redis List
func (c *Cache) LPop(key string) (string, error) {
	ret, err := redisClientWrite0.LPop(key).Result()
	return ret, err
}

//LRemove - Remove a Element from Redis List
func (c *Cache) LRemove(key string, value interface{}) (int64, error) {
	// redisClient = redis.NewClient(&redis.Options{
	// 	Addr:     RedisHost,
	// 	Password: "",
	// 	DB:       0,
	// })
	// defer redisClient.Close()
	ret, err := redisClientWrite0.LRem(key, 1, value).Result()
	return ret, err
}

//LRange - Get all Element from Redis List
func (c *Cache) LRange(key string) ([]string, error) {
	ret, err := redisClientRead0.LRange(key, 0, -1).Result()
	return ret, err
}

//LIndex - Get Element's Value from Index of Redis List
func (c *Cache) LIndex(key string, index int64) (string, error) {
	ret, err := redisClientRead0.LIndex(key, index).Result()
	return ret, err
}

//LDel - Delete a Redis List
func (c *Cache) LDel(key ...string) (int64, error) {
	ret, err := redisClientWrite0.Del(key...).Result()
	return ret, err
}

//LKeys - Get all List Keys with pattern
func (c *Cache) LKeys(key string) []string {
	ret := redisClientRead0.Keys(key + "*").Val()
	return ret
}
