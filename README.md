# VNPTIT.KV2.VNPT.Platform:
- mBaaS Component: cung cấp dịch vụ mobile Backend as a Service (giống Google Firebase Realtime Database)
- Realtime Notification: cung cấp dịch vụ Realtime Notification (giống Google Firebase Notification)
- IoT: cung cấp dịch vụ Internet of Things.
- SSO: cung cấp dịch vụ Single Sign On.
- GeoFencing: Cung cấp dịch vụ Realtime GeoFencing.
- Analytics: Cung cấp các dịch vụ Machine Learning & Data Analytics.

## Description:
- Mô hình giao tiếp: (đang cập nhật)
- Thành viên đang phát triển: 
  - Phùng Khắc Tú - tupk@vnpt.vn  (SA & Backend Developer)
  - Nguyễn Tấn Lợi - loinguyen.tan98@gmail.com (Fullstack Developer)
  - Lê Khắc Tiến - tienlk.vnpt@gmail.com (Backend Developer & Tester)
- Liên hệ: Email như trên
- Phương thức hỗ trợ: GET, POST, PUT, DELETE, PATCH
- Ngôn ngữ lập trình: 
  - Backend: Golang, Java.
  - Frontend: HTML, CSS, Javascript.
- Framework: 
  - Backend: Echo Framework, Spring Boot.
  - Frontend: VueJS Framework.
- Thông tin truy cập vào Dashboard: http://203.162.141.98:8080
- Thông tin truy cập vào Swagger: http://203.162.141.98:1323/swagger/index.html

## Prerequisite:
- Backend: 
  - Source code của mBaaS Component cần phải được đặt tại: $GOPATH/src/bitbucket.org/cloud-platform/sme-realtime-database
  - Lấy về toàn bộ Dependency Package với lệnh: "go get" từ parent path

## How to Build & Run:
- Backend: 
  - Development: chạy trực tiếp với lệnh: "go run main.go" từ parent folder.
  - Production: Build Docker Image cho mBaaS Component sử dụng file build.sh

## How to Use:
- Local Environment: 
  - Dashboard: http://localhost:8080
  - API: http://localhost:1323
- Dev Environment:
  - Dashboard: http://203.162.141.98:8080
  - API: http://203.162.141.98:1323


## How to Config

## How to Test:
Công cụ để test: [Postman](https://www.getpostman.com/downloads/). Import Postman Collection trong thư mục testing/postman
