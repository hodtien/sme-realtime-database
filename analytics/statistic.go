package analytics

import (
	"strconv"
	"strings"
	"time"

	"bitbucket.org/cloud-platform/sme-realtime-database/middleware"
	"github.com/labstack/echo"
)

// MonitorAPICall is the middleware function.
func MonitorAPICall(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		userID := c.Request().Header.Get("userid")
		bearer := c.Request().Header.Get("Authorization")
		if userID == "" && bearer == "" {
			return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
		} else if userID == "" && bearer != "" {
			apikey := strings.Split(bearer, " ")[1]
			userID, _, err := middleware.GetUserIDAndDeviceIDByAPIKey(apikey)
			if userID == "" || err != nil {
				return c.JSON(400, map[string]interface{}{"code": "-1", "message": "UserID or DeviceID is Invalid"})
			}
		}

		go func(userID string) {
			currentTime := time.Now().Local()
			timeStampString := currentTime.Format("02-01-2006_15")

			hour := strings.Split(timeStampString, "_")[1]
			date := strings.Split(timeStampString, "_")[0]
			day := strings.Split(date, "-")[0]
			month := strings.Split(date, "-")[1]
			year := strings.Split(date, "-")[2]

			// Thống kê số lượng các API sử dụng theo từng API
			db.ZIncrBy(0, "stat$"+userID+"$"+c.Request().URL.Path, "dt_"+currentTime.Format("02-01-2006"), 1)

			// Thống kê trạng thái API trả về theo từng API
			db.ZIncrBy(0, "stat$"+userID+"$"+c.Request().URL.Path, strconv.Itoa(c.Response().Status), 1)

			// Thống kê trong 1 ngày số lượng API được sử dụng theo giờ
			db.ZIncrBy(0, "stat$"+userID+"$"+"dt_"+day+"-"+month+"-"+year, hour, 1)

			// Thống kê trong 1 tháng số lượng API được sử dụng theo ngày
			db.ZIncrBy(0, "stat$"+userID+"$"+"dt_"+month+"-"+year, day, 1)

			// Thống kê trong 1 năm số lượng API được sử dụng theo tháng
			db.ZIncrBy(0, "stat$"+userID+"$"+"dt_"+year, month, 1)

		}(userID)

		if err := next(c); err != nil {
			c.Error(err)
		}
		return nil
	}

}
