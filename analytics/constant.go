package analytics

import "bitbucket.org/cloud-platform/sme-realtime-database/repository"

//Statistic - Count API struct
type Statistic struct {
	API   string  `json:"api"`
	Date  string  `json:"date"`
	Count float64 `json:"count"`
}

//StatsByDay - Thống kê API sử dụng theo ngày
type StatsByDay struct {
	Hour   string `json:"hour"`
	APIUse string `json:"api_use"`
}

//StatsByMonth - Thống kê API sử dụng theo tháng
type StatsByMonth struct {
	Day    string `json:"day"`
	APIUse string `json:"api_use"`
}

//StatsByYear - Thống kê API sử dụng theo năm
type StatsByYear struct {
	Month  string `json:"month"`
	APIUse string `json:"api_use"`
}

var db repository.Cache
