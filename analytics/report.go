package analytics

import (
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo"
)

//StatsSummary - StatsSummary
func StatsSummary(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, "UserID or DeviceID is Invalid")
	}
	keys := db.ZKeys(0, "stat$"+userID+"$"+"/api/core/v1/")
	var totalQuery float64 = 0
	var totalSuccess float64 = 0
	var totalError float64 = 0
	for _, key := range keys {
		redisZ := db.ZRangeWithScore(0, key, 0, -1)
		for _, redisz := range redisZ {
			if strings.HasPrefix(redisz.Member.(string), "dt_") {
				totalQuery = totalSuccess + totalError //totalQuery + redisz.Score
			}
			if redisz.Member.(string) == "200" {
				totalSuccess = totalSuccess + redisz.Score
			}
			if redisz.Member.(string) == "500" {
				totalError = totalError + redisz.Score
			}
		}
	}
	result := map[string]interface{}{
		"total_query":   totalQuery,
		"total_success": totalSuccess,
		"total_error":   totalError,
	}
	return c.JSON(http.StatusOK, result)
}

// StatsDetail is the endpoint to get stats.
func StatsDetail(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, "UserID or DeviceID is Invalid")
	}
	param := c.Param("date")
	keys := db.ZKeys(0, "stat$"+userID+"$"+"/api/core/v1/")
	var results []*Statistic
	for _, key := range keys {
		count, _ := db.ZScore(0, key, param)
		result := &Statistic{
			API:   key,
			Date:  param,
			Count: count,
		}
		results = append(results, result)
	}
	return c.JSON(http.StatusOK, results)
}

//StatsByDayReport - Xuất báo cáo số API sử dụng trong ngày (24h)
func StatsByDayReport(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, "UserID or DeviceID is Invalid")
	}
	day := c.Param("day")
	re := regexp.MustCompile("(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d)")
	if !re.MatchString(day) {
		return c.JSON(200, "Format Day Report is invalid")
	}

	statsByDay := make([]StatsByDay, 24)
	for i := 0; i < 24; i++ {
		statsByDay[i].Hour = strconv.Itoa(i)
		statsByDay[i].APIUse = "0"
	}

	redisKey := "stat$" + userID + "$" + "dt_" + day
	redisZ := db.ZRangeWithScore(0, redisKey, 0, -1)
	for _, redisz := range redisZ {
		index, err := strconv.Atoi(redisz.Member.(string))
		if err != nil {
			fmt.Println(err)
		}
		statsByDay[index].APIUse = fmt.Sprintf("%.0f", redisz.Score)
	}
	return c.JSON(200, statsByDay)
}

//StatsByMonthReport - Xuất báo cáo số API sử dụng trong tháng (30 ngày)
func StatsByMonthReport(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, "UserID or DeviceID is Invalid")
	}
	month := c.Param("month")
	re := regexp.MustCompile("(0?[1-9]|1[012])-((19|20)\\d\\d)")
	if !re.MatchString(month) {
		return c.JSON(200, "Format Month Report is invalid")
	}
	monthYear := strings.Split(month, "-")
	m, err := strconv.Atoi(monthYear[0])
	y, err := strconv.Atoi(monthYear[1])
	if err != nil {
		return c.JSON(200, "Format Month Report is invalid")
	}
	lastDayInMonth := time.Date(y, time.Month(m+1), 0, 0, 0, 0, 0, time.Local).Day()
	statsByMonth := make([]StatsByMonth, lastDayInMonth)
	for i := 0; i < lastDayInMonth; i++ {
		statsByMonth[i].Day = strconv.Itoa(i + 1)
		statsByMonth[i].APIUse = "0"
	}

	redisKey := "stat$" + userID + "$" + "dt_" + month
	redisZ := db.ZRangeWithScore(0, redisKey, 0, -1)
	for _, redisz := range redisZ {
		index, err := strconv.Atoi(redisz.Member.(string))
		if err != nil {
			fmt.Println(err)
		}
		index = index - 1
		statsByMonth[index].APIUse = fmt.Sprintf("%.0f", redisz.Score)
	}
	return c.JSON(200, statsByMonth)
}

//StatsByYearReport - Xuất báo cáo số API sử dụng trong năm (12 tháng)
func StatsByYearReport(c echo.Context) error {
	userID := c.Request().Header.Get("userid")
	if userID == "" {
		return c.JSON(400, "UserID or DeviceID is Invalid")
	}
	year := c.Param("year")
	re := regexp.MustCompile("((19|20)\\d\\d)")
	if !re.MatchString(year) {
		return c.JSON(200, "Format Year Report is invalid")
	}

	statsByYear := make([]StatsByYear, 12)
	for i := 0; i < 12; i++ {
		statsByYear[i].Month = strconv.Itoa(i + 1)
		statsByYear[i].APIUse = "0"
	}

	redisKey := "stat$" + userID + "$" + "dt_" + year
	redisZ := db.ZRangeWithScore(0, redisKey, 0, -1)
	for _, redisz := range redisZ {
		index, err := strconv.Atoi(redisz.Member.(string))
		if err != nil {
			fmt.Println(err)
		}
		index = index - 1
		statsByYear[index].APIUse = fmt.Sprintf("%.0f", redisz.Score)
	}
	return c.JSON(200, statsByYear)
}

//HTTPClientGetStatsContainer - Http Client Get Statistic Container
func HTTPClientGetStatsContainer(bucket string) *http.Response {
	swiftToken := db.GetSwiftToken()
	storageURL := db.GetSwiftStorageURL()
	req, err := http.NewRequest("HEAD", storageURL+"/"+bucket, nil)
	if err != nil {
		fmt.Println("Error reading request. ", err)
	}
	req.Header.Set("X-Auth-Token", swiftToken)

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error reading response. ", err)
		// return errors.New("Error reading response")
	}
	defer resp.Body.Close()
	return resp
}
