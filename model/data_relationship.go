package model

type (
	// DataRelationship - DataRelationship
	DataRelationship struct {
		ParentRecordID string `json:"parent_record_id" validate:"required"`
		ParentBucketID string `json:"parent_bucket_id" validate:"required"`
	}
)
