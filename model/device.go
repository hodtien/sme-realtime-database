package model

type (
	// Device - Device Structure
	Device struct {
		ID           string `json:"id"`
		Name         string `json:"name" validate:"required"`
		CreatingDate string `json:"creatingDate"`
		Description  string `json:"description"`
		APIKey       string `json:"apiKey"`
	}
)
