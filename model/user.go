package model

type (
	// UserMgnt - UserMgnt Structure
	UserMgnt struct {
		UserID      string `json:"user_id"`
		Username    string `json:"username" validate:"required"`
		Password    string `json:"password" validate:"required"`
		Email       string `json:"email" validate:"required"`
		Address     string `json:"address" validate:"required"`
		PhoneNumber string `json:"phone_number" validate:"required"`
		Role        string `json:"roles"`
		FirstName   string `json:"first_name" validate:"required"`
		LastName    string `json:"last_name" validate:"required"`
	}

	// DataLogin - DataLogin
	DataLogin struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
)
