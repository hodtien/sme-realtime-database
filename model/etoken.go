package model

type (
	// EToken - EToken Structure
	EToken struct {
		AppID   string `json:"app_id"`
		AppName string `json:"app_name"`
	}
)
