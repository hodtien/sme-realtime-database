package model

type (

	// Bucket - Bucket Structure
	Bucket struct {
		ID              string `json:"id"`
		Name            string `json:"name" validate:"required"`
		CreatingDate    string `json:"creatingDate"`
		Description     string `json:"description"`
		CreatedByDevice string `json:"created_by_device"`
		CreatedByUser   string `json:"created_by_user"`
	}
)
